## msm8937_64-userdebug 7.1.2 N2G47H 7561 release-keys
- Manufacturer: 10or
- Platform: msm8937
- Codename: E
- Brand: 10.or
- Flavor: msm8937_64-userdebug
- Release Version: 7.1.2
- Id: N2G47H
- Incremental: HOLLAND2_S100_170309170309
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 10.or/E/E:7.1.2/N2G47H/10or_E_V1_0_82_debug:user/release-keys
- OTA version: 
- Branch: msm8937_64-userdebug-7.1.2-N2G47H-7561-release-keys
- Repo: 10.or_e_dump_75


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
