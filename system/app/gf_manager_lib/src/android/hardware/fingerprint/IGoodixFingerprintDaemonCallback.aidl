package android.hardware.fingerprint;

interface IGoodixFingerprintDaemonCallback {
    void onEnrollResult(long deviceId, int fingerId, int groupId, int remaining);
    void onAcquired(long deviceId, int acquiredInfo);
    void onAuthenticated(long deviceId, int fingerId, int groupId, in byte[] hat);
    void onError(long deviceId, int error);
    void onRemoved(long deviceId, int fingerId, int groupId);
    void onEnumerate(long deviceId, in int [] fingerIds, in int [] groupIds);
    void onTestCmd(long deviceId, int cmdId, in byte[] result);
    void onHbdData(long deviceId, int heartBeatRate, int status, in int[] displayData);
    void onDump(long deviceId, int cmdId, in byte[] data);
    void onAuthenticatedFido(long devId, int fingerId, in byte[] uvtData);
}
