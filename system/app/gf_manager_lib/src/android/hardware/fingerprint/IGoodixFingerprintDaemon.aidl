package android.hardware.fingerprint;

import android.hardware.fingerprint.IGoodixFingerprintDaemonCallback;

interface IGoodixFingerprintDaemon {
    int authenticate(long sessionId, int groupId);
    int stopAuthentication();
    int enroll(in byte[] token, int groupId, int timeout);
    int stopEnrollment();
    long preEnroll();
    int remove(int fingerId, int groupId);
    long getAuthenticatorId();
    int setActiveGroup(int groupId, in byte[] path);
    long openHal();
    int closeHal();
    void init(IGoodixFingerprintDaemonCallback callback);
    int postEnroll();
    int setSafeClass(int safeClass);
    int navigate(int navMode);
    int stopNavigation();
    int enableFingerprintModule(byte enable);
    int cameraCapture();
    int stopCameraCapture();
    int enableFfFeature(byte enable);
    void testInit(IGoodixFingerprintDaemonCallback callback);
    int testCmd(int cmdId, in byte[] param);
    int screenOn();
    int screenOff();
    int startHbd();
    int stopHbd();
    int enumerate();
    int reset_lockout(in byte[] token);
    int enableBioAssayFeature(byte enableFlag);
    void syncFingerList();
    int dumpCmd(int cmdId, in byte[] param);
    int authenticateFido(int groupId, in byte[] aaid, in byte[] finalChallenge);
    int stopAuthenticateFido();
    int isIdValid(int groupId, int fingerId);
    int[] getIdList(int groupId);
    int invokeFidoCommand(in byte[] inBuf, out byte[] outBuf);
}
