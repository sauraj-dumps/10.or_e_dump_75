package com.goodix.fingerprint.service;

interface IGoodixFingerprintDumpCallback {
    void onDump(int cmdId, in byte[] data);
}
