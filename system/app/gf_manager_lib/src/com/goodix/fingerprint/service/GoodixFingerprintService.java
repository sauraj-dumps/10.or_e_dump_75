
package com.goodix.fingerprint.service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.GFDevice;
import com.goodix.fingerprint.utils.TestResultParser;

import android.app.AppOpsManager;
import android.content.Context;
import android.hardware.fingerprint.IGoodixFingerprintDaemon;
import android.hardware.fingerprint.IGoodixFingerprintDaemonCallback;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class GoodixFingerprintService extends IGoodixFingerprintInterface.Stub implements
IBinder.DeathRecipient {
    private static final String TAG = "GoodixFingerprintService";

    private static final String GOODIXFINGERPRINTD = "android.hardware.fingerprint.IGoodixFingerprintDaemon";
    public static final String MANAGE_FINGERPRINT = "android.permission.MANAGE_FINGERPRINT";
    public static final String USE_FINGERPRINT = "android.permission.USE_FINGERPRINT";

    private static final int TEST_GET_CONFIG_MAX_RETRY_TIMES = 10;

    private Context mContext = null;
    private Handler mHandler = null;
    private IGoodixFingerprintDaemon mDaemon = null;
    private IGoodixFingerprintCallback mCallback = null;
    private IGoodixFingerprintDumpCallback mDumpCallback = null;
    private int mGetConfigRetryCount = 0;
    private GFConfig mConfig = new GFConfig();
    private GFDevice mDevice = new GFDevice();

    private AppOpsManager mAppOps = null;
    private String mOpPackageName = null;
    private int mOpUseFingerprint = -1;

    public GoodixFingerprintService(Context context) {
        mContext = context;
        mHandler = new Handler(context.getMainLooper());

        mAppOps = (AppOpsManager) (context.getSystemService(Context.APP_OPS_SERVICE));
        mOpPackageName = getAppOpPackageName();
        mOpUseFingerprint = getOpUseFingerprint();

        testCmd(Constants.CMD_TEST_GET_CONFIG, null, mOpPackageName);
        mGetConfigRetryCount++;
    }

    private String getAppOpPackageName() {
        String opPackageName = null;

        if (mContext == null) {
            return null;
        }

        try {
            Method getOpPackageName = mContext.getClass().getMethod("getOpPackageName");
            getOpPackageName.setAccessible(true);
            opPackageName = (String) getOpPackageName.invoke(mContext);
        } catch (NoSuchMethodException e) {
        } catch (IllegalAccessException e) {
        } catch (IllegalArgumentException e) {
        } catch (InvocationTargetException e) {
        }

        return opPackageName;
    }

    private int getOpUseFingerprint() {
        try {
            Class c = AppOpsManager.class;
            Field f = c.getDeclaredField("OP_USE_FINGERPRINT");
            return (f.getInt(null));
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        } catch (IllegalArgumentException e) {
        }
        return -1;
    }

    void checkPermission(String permission) {
        mContext.enforceCallingOrSelfPermission(permission,
                "Must have " + permission + " permission.");
    }

    private boolean canUseFingerprint(String opPackageName) {
        checkPermission(USE_FINGERPRINT);

        try {
            Method noteOp = mAppOps.getClass().getMethod("noteOp", new Class[] {
                    int.class, int.class, String.class
            });

            noteOp.setAccessible(true);
            Integer mode = (Integer) (noteOp.invoke(mAppOps, mOpUseFingerprint,
                    Binder.getCallingUid(), opPackageName));
            return mode.intValue() == AppOpsManager.MODE_ALLOWED;
        } catch (NoSuchMethodException e) {
        } catch (InvocationTargetException e) {
        } catch (IllegalAccessException e) {
        }
        return false;
    }

    private IGoodixFingerprintDaemon getFingerprintDaemon() {
        Log.d(TAG, "getFingerprintDaemon()");
        if (mDaemon == null) {
            try {
                Class<?> serviceManagerClazz = Class.forName("android.os.ServiceManager");
                Method method = serviceManagerClazz.getMethod("getService", String.class);
                IBinder binder = (IBinder) method.invoke(null, GOODIXFINGERPRINTD);
                mDaemon = IGoodixFingerprintDaemon.Stub.asInterface(binder);
                mDaemon.asBinder().linkToDeath(this, 0);
            } catch (Exception e) {
            }

            if (mDaemon != null) {
                try {
                    mDaemon.asBinder().linkToDeath(this, 0);
                    mDaemon.testInit(mDaemonCallback);
                } catch (RemoteException e) {
                }
            }
        }

        return mDaemon;
    }

    @Override
    public void initCallback(IGoodixFingerprintCallback callback, String opPackageName) {
        Log.d(TAG, "initCallback");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mCallback = callback;
    }

    @Override
    public void testCmd(final int cmdId, final byte[] param, String opPackageName) {
        Log.d(TAG, "testCmd " + cmdId);
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "testCmd: no goodixfingeprintd!");
                    return;
                }

                try {
                    final int result = daemon.testCmd(cmdId, param);

                    if (result != 0 && Constants.CMD_TEST_GET_CONFIG == cmdId
                            && mGetConfigRetryCount < TEST_GET_CONFIG_MAX_RETRY_TIMES) {
                        mHandler.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                testCmd(Constants.CMD_TEST_GET_CONFIG, null, mOpPackageName);
                                mGetConfigRetryCount++;
                                Log.i(TAG, "CMD_TEST_GET_CONFIG retry count : " + mGetConfigRetryCount);
                            }

                        }, 1000);
                    }
                } catch (RemoteException e) {
                    Log.e(TAG, "testCmd RemoteException");
                }
            }

        });
    }

    @Override
    public int testSync(int cmdId, byte[] param, String opPackageName) {
        Log.d(TAG, "testSync");
        if (!canUseFingerprint(opPackageName)) {
            return 0;
        }

        IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
        if (daemon == null) {
            Log.e(TAG, "testSync: no goodixfingeprintd!");
            return 0;
        }

        try {
            return daemon.testCmd(cmdId, param);
        } catch (RemoteException e) {
            Log.e(TAG, "testSync RemoteException");
        }
        return 0;
    }

    @Override
    public void setSafeClass(final int safeClass, String opPackageName) {
        Log.d(TAG, "setSafeClass");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "setSafeClass: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.setSafeClass(safeClass);
                    mDevice.mSafeClass = safeClass;
                } catch (RemoteException e) {
                    Log.e(TAG, "setSafeClass RemoteException");
                }
            }

        });
    }

    @Override
    public void navigate(final int navMode, String opPackageName) {
        Log.d(TAG, "navigate");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "navigate: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.navigate(navMode);
                } catch (RemoteException e) {
                    Log.e(TAG, "navigate RemoteException");
                }
            }
        }, 100);
    }

    @Override
    public void stopNavigation(String opPackageName) {
        Log.d(TAG, "stopNavigation");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "stopNavigation: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.stopNavigation();
                } catch (RemoteException e) {
                    Log.e(TAG, "stopNavigation RemoteException");
                }
            }

        });
    }

    @Override
    public void enableFingerprintModule(final boolean enable, String opPackageName) {
        Log.d(TAG, "enableFingerprintModule");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "enableFingerprintModule: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.enableFingerprintModule(enable ? (byte) 1 : (byte) 0);
                } catch (RemoteException e) {
                    Log.e(TAG, "enableFingerprintModule RemoteException");
                }
            }

        });
    }

    @Override
    public void cameraCapture(String opPackageName) {
        Log.d(TAG, "cameraCapture");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "cameraCapture: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.cameraCapture();
                } catch (RemoteException e) {
                    Log.e(TAG, "cameraCapture RemoteException");
                }
            }
        }, 100);
    }

    @Override
    public void stopCameraCapture(String opPackageName) {
        Log.d(TAG, "stopCameraCapture");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "stopCameraCapture: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.stopCameraCapture();
                } catch (RemoteException e) {
                    Log.e(TAG, "stopCameraCapture RemoteException");
                }
            }

        });
    }

    @Override
    public void enableFfFeature(final boolean enable, String opPackageName) {
        Log.d(TAG, "enableFfFeature");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "enableFfFeature: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.enableFfFeature(enable ? (byte) 1 : (byte) 0);
                } catch (RemoteException e) {
                    Log.e(TAG, "enableFfFeature RemoteException");
                }
            }

        });
    }

    @Override
    public void screenOn(String opPackageName) {
        Log.d(TAG, "screenOn");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "screenOn: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.screenOn();
                } catch (RemoteException e) {
                    Log.e(TAG, "screenOn RemoteException");
                }
            }

        });
    }

    @Override
    public void screenOff(String opPackageName) {
        Log.d(TAG, "screenOff");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "screenOff: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.screenOff();
                } catch (RemoteException e) {
                    Log.e(TAG, "screenOff RemoteException");
                }
            }

        });
    }

    @Override
    public GFConfig getConfig(String opPackageName) {
        if (!canUseFingerprint(opPackageName)) {
            return null;
        }

        return new GFConfig(mConfig);
    }

    @Override
    public GFDevice getDevice(String opPackageName) {
        if (!canUseFingerprint(opPackageName)) {
            return null;
        }

        return new GFDevice(mDevice);
    }

    @Override
    public void startHbd(String opPackageName) {
        Log.d(TAG, "startHbd");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "startHbd: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.startHbd();
                } catch (RemoteException e) {
                    Log.e(TAG, "startHbd RemoteException");
                }
            }

        });
    }

    @Override
    public void stopHbd(String opPackageName) {
        Log.d(TAG, "stopHbd");
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "stopHbd: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.stopHbd();
                } catch (RemoteException e) {
                    Log.e(TAG, "stopHbd RemoteException");
                }
            }

        });
    }

    @Override
    public void dump(IGoodixFingerprintDumpCallback callback, String opPackageName)
            throws RemoteException {
        Log.d(TAG, "dump");
        mDumpCallback = callback;
        dumpCmd(Constants.CMD_DUMP_DATA, null, opPackageName);
    }

    @Override
    public void cancelDump(String opPackageName) throws RemoteException {
        Log.d(TAG, "cancelDump");
        mDumpCallback = null;
        dumpCmd(Constants.CMD_CANCEL_DUMP_DATA, null, opPackageName);
    }

    @Override
    public void dumpCmd(final int cmdId, final byte[] param, String opPackageName) throws RemoteException {
        Log.d(TAG, "dumpCmd " + cmdId);
        if (!canUseFingerprint(opPackageName)) {
            return;
        }

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
                if (daemon == null) {
                    Log.e(TAG, "dumpCmd: no goodixfingeprintd!");
                    return;
                }

                try {
                    daemon.dumpCmd(cmdId, param);
                } catch (RemoteException e) {
                    Log.e(TAG, "dumpCmd RemoteException");
                }
            }

        });
    }

    @Override
    public void binderDied() {
        Log.v(TAG, "binderDied");
        mDaemon = null;
    }

    @Override
    public int authenticateFido(byte[] aaid, byte[] finalChallenge, String opPackageName) {
        Log.d(TAG, "authenticateFido");
        if (!canUseFingerprint(opPackageName)) {
            return -1;
        }

        IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
        if (null == daemon) {
            Log.e(TAG, "authenticateFido:no goodixfingerprintd");
            return -1;
        }

        try {
            return daemon.authenticateFido(0, aaid, finalChallenge);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return -1;
    }

    @Override
    public int stopAuthenticateFido(String opPackageName) {
        Log.d(TAG, "cancelAuthenticateFido");
        if (!canUseFingerprint(opPackageName)) {
            return -1;
        }

        IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
        if (null == daemon) {
            Log.e(TAG, "cancelAuthenticateFido:no goodixfingerprintd");
            return -1;
        }

        try {
            return daemon.stopAuthenticateFido();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return -1;
    }

    @Override
    public int isIdValid(int fingerId, String opPackageName) {
        Log.d(TAG, "isIdInvalid");
        if (!canUseFingerprint(opPackageName)) {
            return -1;
        }

        IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
        if (null == daemon) {
            Log.e(TAG, "isIdInvalid:no goodixfingerprintd");
            return -1;
        }

        try {
            return daemon.isIdValid(0, fingerId);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return -1;
    }

    @Override
    public int[] getIdList(String opPackageName) {
        Log.d(TAG, "getIdList");
        if (!canUseFingerprint(opPackageName)) {
            return null;
        }

        IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
        if (null == daemon) {
            Log.e(TAG, "getIdList:no goodixfingerprintd");
            return null;
        }

        try {
            return daemon.getIdList(0);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int invokeFidoCommand(byte[] inBuf, byte[] outBuf){
        Log.d(TAG, "invokeFidoCommand");
        IGoodixFingerprintDaemon daemon = getFingerprintDaemon();
        if (null == daemon) {
            Log.e(TAG, "invokeFidoCommand:no goodixfingerprintd");
            return -1;
        }

        try {
            return daemon.invokeFidoCommand(inBuf, outBuf);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private IGoodixFingerprintDaemonCallback mDaemonCallback = new IGoodixFingerprintDaemonCallback.Stub() {
        @Override
        public void onEnrollResult(long deviceId, int fingerId, int groupId, int remaining) {

        }

        @Override
        public void onAcquired(long deviceId, int acquiredInfo) {

        }

        @Override
        public void onAuthenticated(long deviceId, int fingerId, int groupId, byte[] hat) {

        }

        @Override
        public void onError(long deviceId, int error) {

        }

        @Override
        public void onRemoved(long deviceId, int fingerId, int groupId) {

        }

        @Override
        public void onEnumerate(long deviceId, int[] fingerIds, int[] groupIds) {

        }

        @Override
        public void onTestCmd(long deviceId, int cmdId, byte[] result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (Constants.CMD_TEST_GET_CONFIG == cmdId || Constants.CMD_TEST_SET_CONFIG == cmdId) {
                HashMap<Integer, Object> testResult = TestResultParser.parser(result);
                if (testResult == null) {
                    return;
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
                    int errCode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
                    if (errCode > 0) {
                        return;
                    }
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_CHIP_TYPE)) {
                    mConfig.mChipType = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_CHIP_TYPE);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_CHIP_SERIES)) {
                    mConfig.mChipSeries = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_CHIP_SERIES);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_MAX_FINGERS)) {
                    mConfig.mMaxFingers = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_MAX_FINGERS);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_MAX_FINGERS_PER_USER)) {
                    mConfig.mMaxFingersPerUser = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_MAX_FINGERS_PER_USER);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_SUPPORT_KEY_MODE)) {
                    mConfig.mSupportKeyMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_KEY_MODE);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_SUPPORT_FF_MODE)) {
                    mConfig.mSupportFFMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_FF_MODE);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_SUPPORT_POWER_KEY_FEATURE)) {
                    mConfig.mSupportPowerKeyFeature = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_POWER_KEY_FEATURE);
                }

                if (testResult
                        .containsKey(TestResultParser.TEST_TOKEN_FORBIDDEN_UNTRUSTED_ENROLL)) {
                    mConfig.mForbiddenUntrustedEnroll = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_FORBIDDEN_UNTRUSTED_ENROLL);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_FORBIDDEN_ENROLL_DUPLICATE_FINGERS)) {
                    mConfig.mForbiddenEnrollDuplicateFingers = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_FORBIDDEN_ENROLL_DUPLICATE_FINGERS);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_SUPPORT_BIO_ASSAY)) {
                    mConfig.mSupportBioAssay = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_BIO_ASSAY);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_SUPPORT_PERFORMANCE_DUMP)) {
                    mConfig.mSupportPerformanceDump = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_PERFORMANCE_DUMP);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_SUPPORT_NAV_MODE)) {
                    mConfig.mSupportNavMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_NAV_MODE);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_NAV_DOUBLE_CLICK_TIME)) {
                    mConfig.mNavDoubleClickTime = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_NAV_DOUBLE_CLICK_TIME);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_NAV_LONG_PRESS_TIME)) {
                    mConfig.mNavLongPressTime = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_NAV_LONG_PRESS_TIME);
                }

                if (testResult.containsKey(TestResultParser.TEST_TOKEN_ENROLLING_MIN_TEMPLATES)) {
                    mConfig.mEnrollingMinTemplates = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_ENROLLING_MIN_TEMPLATES);
                }

                if (testResult
                        .containsKey(TestResultParser.TEST_TOKEN_VALID_IMAGE_QUALITY_THRESHOLD)) {
                    mConfig.mValidImageQualityThreshold = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_VALID_IMAGE_QUALITY_THRESHOLD);
                }

                if (testResult
                        .containsKey(TestResultParser.TEST_TOKEN_VALID_IMAGE_AREA_THRESHOLD)) {
                    mConfig.mValidImageAreaThreshold = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_VALID_IMAGE_AREA_THRESHOLD);
                }

                if (testResult
                        .containsKey(TestResultParser.TEST_TOKEN_DUPLICATE_FINGER_OVERLAY_SCORE)) {
                    mConfig.mDuplicateFingerOverlayScore = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_DUPLICATE_FINGER_OVERLAY_SCORE);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_INCREASE_RATE_BETWEEN_STITCH_INFO)) {
                    mConfig.mIncreaseRateBetweenStitchInfo = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_INCREASE_RATE_BETWEEN_STITCH_INFO);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT)) {
                    mConfig.mScreenOnAuthenticateFailRetryCount = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT)) {
                    mConfig.mScreenOffAuthenticateFailRetryCount = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_AUTHENTICATE_ORDER)) {
                    mConfig.mAuthenticateOrder = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_AUTHENTICATE_ORDER);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE)) {
                    mConfig.mReissueKeyDownWhenEntryFfMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE)) {
                    mConfig.mReissueKeyDownWhenEntryImageMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE);
                }

                if (testResult
                        .containsKey(TestResultParser.TEST_TOKEN_SUPPORT_SENSOR_BROKEN_CHECK)) {
                    mConfig.mSupportSensorBrokenCheck = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_SENSOR_BROKEN_CHECK);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR)) {
                    mConfig.mBrokenPixelThresholdForDisableSensor = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY)) {
                    mConfig.mBrokenPixelThresholdForDisableStudy = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY);
                }

                if (testResult
                        .containsKey(TestResultParser.TEST_TOKEN_BAD_POINT_TEST_MAX_FRAME_NUMBER)) {
                    mConfig.mBadPointTestMaxFrameNumber = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_BAD_POINT_TEST_MAX_FRAME_NUMBER);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE)) {
                    mConfig.mReportKeyEventOnlyEnrollAuthenticate = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE)) {
                    mConfig.mRequireDownAndUpInPairsForImageMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE)) {
                    mConfig.mRequireDownAndUpInPairsForFFMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE)) {
                    mConfig.mRequireDownAndUpInPairsForKeyMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE)) {
                    mConfig.mRequireDownAndUpInPairsForNavMode = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_SUPPORT_SET_SPI_SPEED_IN_TEE)) {
                    mConfig.mSupportSetSpiSpeedInTEE = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_SET_SPI_SPEED_IN_TEE);
                }

                if (testResult.containsKey(
                        TestResultParser.TEST_TOKEN_SUPPORT_FRR_ANALYSIS)) {
                    mConfig.mSupportFrrAnalysis = (Integer) testResult
                            .get(TestResultParser.TEST_TOKEN_SUPPORT_FRR_ANALYSIS);
                }

                if (mConfig.mMaxFingers < mConfig.mMaxFingersPerUser) {
                    mConfig.mMaxFingers = mConfig.mMaxFingersPerUser;
                }

                if (Constants.CMD_TEST_GET_CONFIG == cmdId) {
                    return;
                }
            }

            if (mCallback != null) {
                try {
                    mCallback.onTestCmd(cmdId, result);
                } catch (RemoteException e) {
                }
            }
        }

        @Override
        public void onHbdData(long deviceId, int heartBeatRate, int status, int[] displayData) {
            Log.d(TAG, "onHbdData");
            if (mCallback != null) {
                try {
                    mCallback.onHbdData(heartBeatRate, status, displayData);
                } catch (RemoteException e) {
                }
            }
        }

        @Override
        public void onDump(long deviceId, int cmdId, byte[] data) {
            Log.d(TAG, "onDump");
            if (mDumpCallback != null) {
                try {
                    mDumpCallback.onDump(cmdId, data);
                } catch (RemoteException e) {
                }
            }
        }

        @Override
        public void onAuthenticatedFido(long devId, int fingerId, byte[] uvtData) {
            Log.d(TAG, "onAuthenticatedFido, fingerId:" + fingerId);
            if (mCallback != null) {
                try {
                    mCallback.onAuthenticateFido(fingerId, uvtData);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "mCallback is null");
            }
        }
    };

}
