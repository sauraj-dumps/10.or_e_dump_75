
package com.goodix.fingerprint.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.GFDevice;
import com.goodix.fingerprint.utils.TestResultParser;

import android.content.Context;
import android.os.CancellationSignal;
import android.os.CancellationSignal.OnCancelListener;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

public class GoodixFingerprintManager {
    private static final String TAG = "GoodixFingerprintManager";
    private Context mContext = null;
    private Handler mHandler = null;
    private IGoodixFingerprintInterface mIGoodixFingerprintInterface = null;
    private ArrayList<TestCmdCallback> mCallbackList = new ArrayList<TestCmdCallback>();

    private static final int MSG_TEST = 1000;
    private static final int MSG_HBD = 1001;
    private static final int MSG_DUMP = 1002;
    private static final int MSG_AUTHENTICATED_FIDO = 1003;

    private HbdCallback mHbdCallback = null;
    private DumpCallback mDumpCallback = null;
    private String mOpPackageName = null;
    private AuthenticateFidoCallback mAuthenticateFidoCallback = null;

    public interface TestCmdCallback {
        public void onTestCmd(int cmdId, HashMap<Integer, Object> result);
    }

    public interface HbdCallback {
        public void onHbdData(int heartBeatRate, int status, int[] displayData);
    }

    public interface DumpCallback {
        public void onDump(int cmdId, HashMap<Integer, Object> data);
    }

    private String getAppOpPackageName() {
        String opPackageName = null;

        if (mContext == null) {
            return null;
        }

        try {
            Method getOpPackageName = mContext.getClass().getMethod("getOpPackageName");
            getOpPackageName.setAccessible(true);
            opPackageName = (String)getOpPackageName.invoke(mContext);
        } catch (NoSuchMethodException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        } catch (IllegalArgumentException e) {
        }

        return opPackageName;
    }

    public static interface AuthenticateFidoCallback {
        void onResult(int result, byte[] uvt, byte[] fingerId);
    }

    public int authenticateFido(AuthenticateFidoCallback callback, byte[] aaid, byte[] finalChallenge) {
        Log.d(TAG, "authenticateFido");
        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return -1;
        }

        if (null == callback) {
            Log.e(TAG, "authenticate fido callback is null");
            return -1;
        }

        if (null == aaid || null == finalChallenge) {
            Log.e(TAG, "aaid or finalChallenge is null");
            return -1;
        }

        mAuthenticateFidoCallback = callback;

        try {
            Log.d(TAG, "authenticateFido, register callback");
            mIGoodixFingerprintInterface.initCallback(mGoodixFingerprintCallback, mOpPackageName);
            return mIGoodixFingerprintInterface.authenticateFido(aaid, finalChallenge, mOpPackageName);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public void stopAuthenticateFido() {
        Log.d(TAG, "stopAuthenticateFido");
        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        mAuthenticateFidoCallback = null;
        try {
            mIGoodixFingerprintInterface.stopAuthenticateFido(mOpPackageName);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public int isIdValid(byte[] fingerId) {
        Log.d(TAG, "isIdValid");
        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return -1;
        }

        try {
            return mIGoodixFingerprintInterface.isIdValid(bytes2int(fingerId), mOpPackageName);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public int[] getIdList() {
        Log.d(TAG, "getIdList");
        try {
            return mIGoodixFingerprintInterface.getIdList(mOpPackageName);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return null;
    }

    public int invokeFidoCommand(byte[] inBuf, byte[] outBuf) {
        Log.d(TAG, "invokeFidoCommand");
        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return -1;
        }

        if (null == inBuf) {
            Log.e(TAG, "invalid parameter, inBuf is NULL");
            return -1;
        }

        if (null == outBuf) {
            Log.e(TAG, "invalid parameter, outBuf is NULL");
            return -1;
        }

        try {
            return mIGoodixFingerprintInterface.invokeFidoCommand(inBuf, outBuf);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public GoodixFingerprintManager(Context context) {
        mContext = context;
        mHandler = new MyHandler(context);
        mOpPackageName = getAppOpPackageName();
        initService();
    }

    private void initService() {
        IBinder binder = null;
        try {
            Class<?> serviceManager = Class.forName("android.os.ServiceManager");
            Log.d(TAG, "success to get ServiceManager");

            Method getService = serviceManager.getMethod("getService", String.class);
            Log.d(TAG, "success to get method:getService");

            binder = (IBinder) getService.invoke(null, Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            Log.d(TAG, "success to getService: " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
        } catch (ClassNotFoundException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        } catch (IllegalArgumentException e) {
        }

        if (binder == null) {
            Log.e(TAG, "failed to getService: " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        mIGoodixFingerprintInterface = IGoodixFingerprintInterface.Stub.asInterface(binder);
    }

    public void registerTestCmdCallback(TestCmdCallback callback) {
        Log.d(TAG, "registerTestCmdCallback");

        // TODO: No necessary to support multi callback
        mCallbackList.clear();

        mCallbackList.add(callback);

        if (mIGoodixFingerprintInterface != null) {
            try {
                mIGoodixFingerprintInterface.initCallback(mGoodixFingerprintCallback, mOpPackageName);
            } catch (RemoteException e) {
            }
        }
    }

    public void unregisterTestCmdCallback(TestCmdCallback callback) {
        Log.d(TAG, "unregisterTestCmdCallback");

        mCallbackList.remove(callback);
    }

    public void registerHbdCallback(HbdCallback callback) {
        Log.d(TAG, "registerHbdCallback");
        mHbdCallback = callback;

        if (mIGoodixFingerprintInterface != null) {
            try {
                mIGoodixFingerprintInterface.initCallback(mGoodixFingerprintCallback, mOpPackageName);
            } catch (RemoteException e) {
            }
        }
    }

    public void unregisterHbdCallback(HbdCallback callback) {
        Log.d(TAG, "unregisterHbdCallback");
        mHbdCallback = null;
    }

    public void testCmd(int cmdId, byte[] param) {
        Log.d(TAG, "testCmd " + cmdId);

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "testCmd no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.testCmd(cmdId, param, mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public int testSync(int cmdId, byte[] param) {
        Log.d(TAG, "testSync");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "testSync no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return 0;
        }

        try {
            return mIGoodixFingerprintInterface.testSync(cmdId, param, mOpPackageName);
        } catch (RemoteException e) {
        }
        return 0;
    }

    public void setSafeClass(int safeClass) {
        Log.d(TAG, "setSafeClass");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "setSafeClass no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.setSafeClass(safeClass, mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void navigate(int navMode) {
        Log.d(TAG, "navigate");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "navigate no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.navigate(navMode, mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void stopNavigation() {
        Log.d(TAG, "stopNavigation");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "stopNavigation no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.stopNavigation(mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void enableFingerprintModule(boolean enable) {
        Log.d(TAG, "enableFingerprintModule");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "enableFingerprintModule no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.enableFingerprintModule(enable, mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void cameraCapture() {
        Log.d(TAG, "cameraCapture");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "cameraCapture no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.cameraCapture(mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void stopCameraCapture() {
        Log.d(TAG, "stopCameraCapture");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "stopCameraCapture no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.stopCameraCapture(mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void enableFfFeature(boolean enable) {
        Log.d(TAG, "enableFfFeature");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "enableFfFeature no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.enableFfFeature(enable, mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void screenOn() {
        Log.d(TAG, "screenOn");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "screenOn no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.screenOn(mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void screenOff() {
        Log.d(TAG, "screenOff");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "screenOff no " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.screenOff(mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void startHbd() {
        Log.d(TAG, "startHbd");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "startHbd null interface " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.startHbd(mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public GFConfig getConfig() {
        Log.d(TAG, "getConfig");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "getConfig null interface " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return null;
        }

        try {
            return mIGoodixFingerprintInterface.getConfig(mOpPackageName);
        } catch (RemoteException e) {
        }

        return null;
    }

    public GFDevice getDevice() {
        Log.d(TAG, "getDevice");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "getDevice null interface " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return null;
        }

        try {
            return mIGoodixFingerprintInterface.getDevice(mOpPackageName);
        } catch (RemoteException e) {
        }

        return null;
    }

    public void stopHbd() {
        Log.d(TAG, "stopHbd");

        if (mIGoodixFingerprintInterface == null) {
            Log.e(TAG, "stopHbd null interface " + Constants.GOODIX_FINGERPRINT_SERVICE_NAME);
            return;
        }

        try {
            mIGoodixFingerprintInterface.stopHbd(mOpPackageName);
        } catch (RemoteException e) {
        }
    }

    public void dump(CancellationSignal cancel, DumpCallback callback) {
        Log.d(TAG, "dump");

        if (callback == null) {
            throw new IllegalArgumentException("Must supply a dump callback");
        }

        if (cancel != null) {
            if (cancel.isCanceled()) {
                Log.w(TAG, "dump already canceled");
                return;
            } else {
                cancel.setOnCancelListener(new OnDumpCancelListener());
            }
        }

        if (mIGoodixFingerprintInterface != null) try {
            mDumpCallback = callback;
            mIGoodixFingerprintInterface.dump(mGoodixFingerprintDumpCallback, mOpPackageName);
        } catch (RemoteException e) {
            Log.w(TAG, "Remote exception in dump: ", e);
        }
    }

    private void cancelDump() {
        Log.d(TAG, "cancelDump");

        if (mIGoodixFingerprintInterface != null) try {
            mIGoodixFingerprintInterface.cancelDump(mOpPackageName);
        } catch (RemoteException e) {
            Log.w(TAG, "Remote exception while canceling dump");
        }
    }

    public void dumpCmd(int cmdId, byte[] param) {
        Log.d(TAG, "dumpCmd");

        if (mIGoodixFingerprintInterface != null) try {
            mIGoodixFingerprintInterface.dumpCmd(cmdId, param, mOpPackageName);
        } catch (RemoteException e) {
            Log.w(TAG, "Remote exception in dumpCmd: ", e);
        }
    }

    private class MyHandler extends Handler {
        private MyHandler(Context context) {
            super(context.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case MSG_TEST:
                    HashMap<Integer, Object> testResult = TestResultParser.parser((byte[]) msg.obj);

                    for (TestCmdCallback callback : mCallbackList) {
                        callback.onTestCmd(msg.arg1, testResult);
                    }
                    break;
                case MSG_HBD:
                    if (null != mHbdCallback) {
                        mHbdCallback.onHbdData(msg.arg1, msg.arg2, (int[]) msg.obj);
                    }
                    break;
                case MSG_DUMP:
                    HashMap<Integer, Object> dumpData = TestResultParser.parser((byte[]) msg.obj);

                    mDumpCallback.onDump(msg.arg1, dumpData);
                    break;
                case MSG_AUTHENTICATED_FIDO:
                    if (null != mAuthenticateFidoCallback) {
                        int fingerId = msg.arg1;
                        byte[] uvtData = (byte[]) msg.obj;
                        int result = 0;
                        if ((fingerId != 0) && (uvtData != null) && (uvtData.length > 0)) {
                            result = 100;// success, accord to fido
                        } else if (fingerId == 0) {
                            result = 103;// mismatch, accord to fido
                        } else if (fingerId != 0 && (uvtData == null || uvtData.length == 0)) {
                            result = 102;// error, accord to fido
                        } else {
                            result = 113;
                        }
                        mAuthenticateFidoCallback.onResult(result, uvtData, int2bytes(fingerId));
                    } else {
                        Log.e(TAG, "handleMessage, mAuthenticateFidoCallback is null");
                    }
            }
        }
    }

    private IGoodixFingerprintCallback.Stub mGoodixFingerprintCallback = new IGoodixFingerprintCallback.Stub() {
        @Override
        public void onTestCmd(final int cmdId, final byte[] result) {
            Log.d(TAG, "onTestCmd");

            mHandler.obtainMessage(MSG_TEST, cmdId, 0, result).sendToTarget();
        }

        @Override
        public void onHbdData(final int heartBeatRate, final int status, final int[] displayData) {
            Log.d(TAG, "onHbdData");

            mHandler.obtainMessage(MSG_HBD, heartBeatRate, status, displayData).sendToTarget();
        }

        @Override
        public void onAuthenticateFido(int fingerId, byte[] uvt) {
            Log.d(TAG, "onAuthenticateFido, fingerId:" + fingerId);
            Message msg = mHandler.obtainMessage(MSG_AUTHENTICATED_FIDO);
            msg.arg1 = fingerId;
            msg.obj = uvt;
            msg.sendToTarget();
        }
    };

    private IGoodixFingerprintDumpCallback.Stub mGoodixFingerprintDumpCallback = new IGoodixFingerprintDumpCallback.Stub() {
        @Override
        public void onDump(final int cmdId, final byte[] data) {
            Log.d(TAG, "onDump");

            mHandler.obtainMessage(MSG_DUMP, cmdId, 0, data).sendToTarget();
        }
    };


    private class OnDumpCancelListener implements OnCancelListener {
        @Override
        public void onCancel() {
            cancelDump();
        }
    }

    public static byte[] int2bytes(int res) {
        byte[] targets = new byte[4];

        targets[0] = (byte) (res & 0xff);
        targets[1] = (byte) ((res >> 8) & 0xff);
        targets[2] = (byte) ((res >> 16) & 0xff);
        targets[3] = (byte) (res >>> 24);
        return targets;
    }

    public static int bytes2int(byte[] res) {
        int targets = (res[0] & 0xff) | ((res[1] << 8) & 0xff00)
                | ((res[2] << 24) >>> 8) | (res[3] << 24);
        return targets;
    }

}
