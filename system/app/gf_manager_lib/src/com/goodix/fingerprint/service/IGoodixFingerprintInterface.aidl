package com.goodix.fingerprint.service;

import com.goodix.fingerprint.service.IGoodixFingerprintCallback;
import com.goodix.fingerprint.service.IGoodixFingerprintDumpCallback;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.GFDevice;

interface IGoodixFingerprintInterface {
    void initCallback(IGoodixFingerprintCallback callback, String opPackageName);
    void testCmd(int cmdId, in byte[] param, String opPackageName);
    int testSync(int cmdId, in byte[] param, String opPackageName);
    void setSafeClass(int safeClass, String opPackageName);
    void navigate(int navMode, String opPackageName);
    void stopNavigation(String opPackageName);
    void enableFingerprintModule(boolean enable, String opPackageName);
    void cameraCapture(String opPackageName);
    void stopCameraCapture(String opPackageName);
    void enableFfFeature(boolean enable, String opPackageName);
    void screenOn(String opPackageName);
    void screenOff(String opPackageName);
    void startHbd(String opPackageName);
    void stopHbd(String opPackageName);
    GFConfig getConfig(String opPackageName);
    GFDevice getDevice(String opPackageName);
    void dump(IGoodixFingerprintDumpCallback callback, String opPackageName);
    void cancelDump(String opPackageName);
    void dumpCmd(int cmdId, in byte[] param, String opPackageName);
    // for fido
    int authenticateFido(in byte[] aaid, in byte[] finalChallenge, String opPackageName);
    int stopAuthenticateFido(String opPackageName);
    int isIdValid(int fingerId, String opPackageName);
    int[] getIdList(String opPackageName);
    int invokeFidoCommand(in byte[] inBuf, out byte[] outBuf);
}
