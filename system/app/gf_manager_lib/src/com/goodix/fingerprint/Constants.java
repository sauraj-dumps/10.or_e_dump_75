
package com.goodix.fingerprint;

public class Constants {

    public static final String GOODIX_FINGERPRINT_SERVICE_NAME = "com.goodix.FingerprintService";

    public static final int GF_CHIP_316M = 0;
    public static final int GF_CHIP_318M = GF_CHIP_316M + 1;
    public static final int GF_CHIP_3118M = GF_CHIP_318M + 1;
    public static final int GF_CHIP_516M = GF_CHIP_3118M + 1;
    public static final int GF_CHIP_518M = GF_CHIP_516M + 1;
    public static final int GF_CHIP_5118M = GF_CHIP_518M + 1;
    public static final int GF_CHIP_816M = GF_CHIP_5118M + 1;
    public static final int GF_CHIP_3266 = GF_CHIP_816M + 1;
    public static final int GF_CHIP_3208 = GF_CHIP_3266 + 1;
    public static final int GF_CHIP_3268 = GF_CHIP_3208 + 1;
    public static final int GF_CHIP_3228 = GF_CHIP_3268 + 1;
    public static final int GF_CHIP_3288 = GF_CHIP_3228 + 1;
    public static final int GF_CHIP_3206 = GF_CHIP_3288 + 1;
    public static final int GF_CHIP_3226 = GF_CHIP_3206 + 1;
    public static final int GF_CHIP_3258 = GF_CHIP_3226 + 1;
    public static final int GF_CHIP_5206 = GF_CHIP_3258 + 1;
    public static final int GF_CHIP_5216 = GF_CHIP_5206 + 1;
    public static final int GF_CHIP_5208 = GF_CHIP_5216 + 1;
    public static final int GF_CHIP_5218 = GF_CHIP_5208 + 1;
    public static final int GF_CHIP_UNKNOWN = GF_CHIP_5218 + 1;


    public static final int ENROLLING_MIN_TEMPLATES_NUM[] = {
            10, // GF_CHIP_316M
            8, // GF_CHIP_318M
            8, // GF_CHIP_3118M
            10, // GF_CHIP_516M
            8, // GF_CHIP_518M
            8, // GF_CHIP_5118M
            10, // GF_CHIP_816M
            8, // GF_CHIP_3266
            8, // GF_CHIP_3208
            8, // GF_CHIP_3268
            8, // GF_CHIP_3228
            8, // GF_CHIP_3288
            8, // GF_CHIP_3206
            8, // GF_CHIP_3226
            8, // GF_CHIP_3258
            10, // GF_CHIP_5206
            10,  // GF_CHIP_5216
            8, // GF_CHIP_5208
            8 // GF_CHIP_5218
    };

    public static final int MAX_TEMPLATES_NUM[] = {
            40, // GF_CHIP_316M
            30, // GF_CHIP_318M
            30, // GF_CHIP_3118M
            40, // GF_CHIP_516M
            30, // GF_CHIP_518M
            30, // GF_CHIP_5118M
            40, // GF_CHIP_816M
            35, // GF_CHIP_3266
            30, // GF_CHIP_3208
            30, // GF_CHIP_3268
            45, // GF_CHIP_3228
            20, // GF_CHIP_3288
            40, // GF_CHIP_3206
            30, // GF_CHIP_3226
            50, // GF_CHIP_3258
            40, // GF_CHIP_5206
            40, // GF_CHIP_5216
            40, // GF_CHIP_5208
            40 // GF_CHIP_5218
    };

    public static final int GF_OSWEGO_M = 0;
    public static final int GF_MILAN_F_SERIES = GF_OSWEGO_M + 1;
    public static final int GF_MILAN_A_SERIES = GF_MILAN_F_SERIES + 1;
    public static final int GF_UNKNOWN_SERIES = GF_MILAN_A_SERIES + 1;

    public static final int GF_SAFE_CLASS_HIGHEST = 0;
    public static final int GF_SAFE_CLASS_HIGH = 1;
    public static final int GF_SAFE_CLASS_MEDIUM = 2;
    public static final int GF_SAFE_CLASS_LOW = 3;
    public static final int GF_SAFE_CLASS_LOWEST = 4;

    public static final int GF_NAV_MODE_NONE = 0x00;
    public static final int GF_NAV_MODE_X    = 0x01;
    public static final int GF_NAV_MODE_Y    = 0x02;
    public static final int GF_NAV_MODE_Z    = 0x04;
    public static final int GF_NAV_MODE_XY   = GF_NAV_MODE_X | GF_NAV_MODE_Y;
    public static final int GF_NAV_MODE_XZ   = GF_NAV_MODE_X | GF_NAV_MODE_Z;
    public static final int GF_NAV_MODE_YZ   = GF_NAV_MODE_Y | GF_NAV_MODE_Z;
    public static final int GF_NAV_MODE_XYZ  = GF_NAV_MODE_XY | GF_NAV_MODE_Z;

    public static final int GF_AUTHENTICATE_BY_USE_RECENTLY = 0;
    public static final int GF_AUTHENTICATE_BY_ENROLL_ORDER = 1;
    public static final int GF_AUTHENTICATE_BY_REVERSE_ENROLL_ORDER = 2;

    public static final int CMD_TEST_ENUMERATE = 0;
    public static final int CMD_TEST_DRIVER = CMD_TEST_ENUMERATE + 1;
    public static final int CMD_TEST_PIXEL_OPEN = CMD_TEST_DRIVER + 1;
    public static final int CMD_TEST_BAD_POINT = CMD_TEST_PIXEL_OPEN + 1;
    public static final int CMD_TEST_PERFORMANCE = CMD_TEST_BAD_POINT + 1;
    public static final int CMD_TEST_SPI_PERFORMANCE = CMD_TEST_PERFORMANCE + 1;
    public static final int CMD_TEST_SPI_TRANSFER = CMD_TEST_SPI_PERFORMANCE + 1;
    public static final int CMD_TEST_SPI = CMD_TEST_SPI_TRANSFER + 1;
    public static final int CMD_TEST_GET_VERSION = CMD_TEST_SPI + 1;
    public static final int CMD_TEST_FRR_FAR_GET_CHIP_TYPE = CMD_TEST_GET_VERSION + 1;
    public static final int CMD_TEST_FRR_FAR_INIT = CMD_TEST_FRR_FAR_GET_CHIP_TYPE + 1;
    public static final int CMD_TEST_FRR_FAR_RECORD_BASE_FRAME = CMD_TEST_FRR_FAR_INIT + 1;
    public static final int CMD_TEST_FRR_FAR_RECORD_ENROLL = CMD_TEST_FRR_FAR_RECORD_BASE_FRAME + 1;
    public static final int CMD_TEST_FRR_FAR_RECORD_AUTHENTICATE = CMD_TEST_FRR_FAR_RECORD_ENROLL + 1;
    public static final int CMD_TEST_FRR_FAR_RECORD_AUTHENTICATE_FINISH = CMD_TEST_FRR_FAR_RECORD_AUTHENTICATE + 1;
    public static final int CMD_TEST_FRR_FAR_PLAY_BASE_FRAME = CMD_TEST_FRR_FAR_RECORD_AUTHENTICATE_FINISH + 1;
    public static final int CMD_TEST_FRR_FAR_PLAY_ENROLL = CMD_TEST_FRR_FAR_PLAY_BASE_FRAME + 1;
    public static final int CMD_TEST_FRR_FAR_PLAY_AUTHENTICATE = CMD_TEST_FRR_FAR_PLAY_ENROLL + 1;
    public static final int CMD_TEST_FRR_FAR_ENROLL_FINISH = CMD_TEST_FRR_FAR_PLAY_AUTHENTICATE + 1;
    public static final int CMD_TEST_CANCEL_FRR_FAR = CMD_TEST_FRR_FAR_ENROLL_FINISH + 1;
    public static final int CMD_TEST_RESET_PIN = CMD_TEST_CANCEL_FRR_FAR + 1;
    public static final int CMD_TEST_CANCEL = CMD_TEST_RESET_PIN + 1;
    public static final int CMD_TEST_GET_CONFIG = CMD_TEST_CANCEL + 1;
    public static final int CMD_TEST_SET_CONFIG = CMD_TEST_GET_CONFIG + 1;
    public static final int CMD_TEST_DOWNLOAD_FW = CMD_TEST_SET_CONFIG + 1;
    public static final int CMD_TEST_DOWNLOAD_CFG = CMD_TEST_DOWNLOAD_FW + 1;
    public static final int CMD_TEST_DOWNLOAD_FWCFG = CMD_TEST_DOWNLOAD_CFG + 1;
    public static final int CMD_TEST_RESET_FWCFG = CMD_TEST_DOWNLOAD_FWCFG + 1;
    public static final int CMD_TEST_SENSOR_VALIDITY = CMD_TEST_RESET_FWCFG + 1;
    public static final int CMD_TEST_RESET_CHIP = CMD_TEST_SENSOR_VALIDITY + 1;
    public static final int CMD_TEST_UNTRUSTED_ENROLL = CMD_TEST_RESET_CHIP + 1;
    public static final int CMD_TEST_UNTRUSTED_AUTHENTICATE = CMD_TEST_UNTRUSTED_ENROLL + 1;
    public static final int CMD_TEST_DELETE_UNTRUSTED_ENROLLED_FINGER = CMD_TEST_UNTRUSTED_AUTHENTICATE + 1;
    public static final int CMD_TEST_CHECK_FINGER_EVENT = CMD_TEST_DELETE_UNTRUSTED_ENROLLED_FINGER + 1;
    public static final int CMD_TEST_BIO_CALIBRATION = CMD_TEST_CHECK_FINGER_EVENT + 1;
    public static final int CMD_TEST_HBD_CALIBRATION = CMD_TEST_BIO_CALIBRATION + 1;
    public static final int CMD_TEST_SPI_RW = CMD_TEST_HBD_CALIBRATION + 1;
    public static final int CMD_TEST_RAWDATA = CMD_TEST_SPI_RW + 1;
    public static final int CMD_TEST_READ_CFG = CMD_TEST_RAWDATA + 1;
    public static final int CMD_TEST_READ_FW = CMD_TEST_READ_CFG + 1;
    public static final int CMD_TEST_FRR_DATABASE_ACCESS = CMD_TEST_READ_FW + 1; //40
    public static final int CMD_TEST_PRIOR_CANCEL = CMD_TEST_FRR_DATABASE_ACCESS + 1; //41
    public static final int CMD_TEST_NOISE = CMD_TEST_PRIOR_CANCEL + 1;

    public static final int CMD_DUMP_DATA = 1000;
    public static final int CMD_CANCEL_DUMP_DATA = CMD_DUMP_DATA + 1;
    public static final int CMD_DUMP_TEMPLATES = CMD_CANCEL_DUMP_DATA + 1;
    public static final int CMD_DUMP_PATH = CMD_DUMP_TEMPLATES + 1;
    public static final int CMD_DUMP_NAV_BASE = CMD_DUMP_PATH + 1;
    public static final int CMD_DUMP_FINGER_BASE = CMD_DUMP_NAV_BASE + 1;

    public static final int DUMP_PATH_SDCARD = 0;
    public static final int DUMP_PATH_DATA = 1;

    public static final int MAX_TEMPLATE_COUNT = 20;
    public static final int MAX_SAMPLE_COUNT = 50;

    public static final int MAX_REAL_SAMPLE_COUNT = 50;
    public static final int MAX_FAKE_SAMPLE_COUNT = 50;

    public class Oswego {
        public static final String TEST_SPI_GFX18 = "GFx18M";
        public static final String TEST_SPI_GFX16 = "GFx16M";

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 25;

        public static final int TEST_BAD_POINT_BAD_PIXEL_NUM = 25;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 4;
        public static final int TEST_BAD_POINT_AVG_DIFF_VAL = 800;
        public static final float TEST_BAD_POINT_ALL_TILT_ANGLE = 0.1f;
        public static final float TEST_BAD_POINT_BLOCK_TILT_ANGLE_MAX = 0.269f;
        public static final int TEST_BAD_POINT_LOCAL_SMALL_BAD_POINT = 10;
        public static final int TEST_BAD_POINT_LOCAL_BIG_BAD_POINT = 5;
    }

    public class MilanF {
        public static final int TEST_SPI_CHIP_ID = 0x002202;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;

        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 45;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
    }

    public class MilanFN {
        public static final int TEST_SPI_CHIP_ID = 0x00220C;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;

        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 45;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
    }

    public class MilanK {
        public static final int TEST_SPI_CHIP_ID = 0x00220A;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;

        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 45;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
    }

    public class MilanJ {
        public static final int TEST_SPI_CHIP_ID = 0x00220E;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;

        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 45;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
    }

    public class MilanE {
        public static final int TEST_SPI_CHIP_ID = 0x002207;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;

        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 45;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
    }

    public class MilanG {
        public static final int TEST_SPI_CHIP_ID = 0x002208;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;

        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 45;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
    }

    public class MilanL {
        public static final int TEST_SPI_CHIP_ID = 0x002205;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;

        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 45;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
    }

    public class MilanH {
        public static final int TEST_SPI_CHIP_ID = 0x00220D;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;

        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 45;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
    }

    public class MilanASeries {
        public static final int TEST_MILAN_A_SERIES_SENSOR_OTP_TYPE_1 = 0x01;
        public static final int TEST_MILAN_A_SERIES_SENSOR_OTP_TYPE_2 = 0x02;
        public static final int TEST_MILAN_A_SERIES_SENSOR_OTP_TYPE_3 = 0x03;
        public static final int TEST_MILAN_A_SERIES_SENSOR_OTP_TYPE_4 = 0x04;
    }

    public class MilanA extends MilanASeries {
        public static final String TEST_SPI_FW_VERSION = "GF52X6";

        public static final long TEST_PERFORMANCE_TOTAL_TIME = 500;
        public static final int TEST_SPI_CHIP_ID = 0x12A;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;
        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 50;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
        public static final int TEST_BAD_POINT_INCIRCLE = 30;

        public static final int TEST_BIO_THRESHOLD_UNTOUCHED = 150;
        public static final int TEST_BIO_THRESHOLD_TOUCHED = 1000;

        public static final int TEST_HBD_THRESHOLD_AVG_MIN = 500;
        public static final int TEST_HBD_THRESHOLD_AVG_MAX = 3500;
        public static final int TEST_HBD_THRESHOLD_ELECTRICITY = 255;

    }

    public class MilanC extends MilanASeries {
        public static final String TEST_SPI_FW_VERSION = "GF52X8";

        public static final long TEST_PERFORMANCE_TOTAL_TIME = 500;
        public static final int TEST_SPI_CHIP_ID = 0x12B;

        public static final int TEST_SENSOR_BAD_POINT_COUNT = 35;
        public static final int TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM = 50;
        public static final int TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM = 15;
        public static final int TEST_BAD_POINT_LOCAL_WORST = 8;
        public static final int TEST_BAD_POINT_INCIRCLE = 30;

        public static final int TEST_BIO_THRESHOLD_UNTOUCHED = 150;
        public static final int TEST_BIO_THRESHOLD_TOUCHED = 1000;

        public static final int TEST_HBD_THRESHOLD_AVG_MIN = 500;
        public static final int TEST_HBD_THRESHOLD_AVG_MAX = 3500;
        public static final int TEST_HBD_THRESHOLD_ELECTRICITY = 255;
    }

    public static final int ALGO_VERSION_INFO_LEN = 64;
    public static final int FW_VERSION_INFO_LEN = 64;
    public static final int TEE_VERSION_INFO_LEN = 72;
    public static final int TA_VERSION_INFO_LEN = 64;
    public static final int VENDOR_ID_LEN = 32;
    public static final int PRODUCTION_DATE_LEN = 32;

    public static final int TEST_CAPTURE_VALID_IMAGE_QUALITY_THRESHOLD = 15;
    public static final int TEST_CAPTURE_VALID_IMAGE_AREA_THRESHOLD = 65;

    public static final String TEST_FW_VERSION_GFX18 = "GFx18M_1.04.04";
    public static final String TEST_FW_VERSION_GFX16 = "GFx16M_1.04.04";

    public static final long TEST_PERFORMANCE_TOTAL_TIME = 400;

    public static final long TEST_TIMEOUT_MS = 30 * 1000;

    public static final String TEST_HISTORY_PATH = "GFTest";

    public static final int AUTO_TEST_TIME_INTERVAL = 5000; // 5 seconds
    public static final int AUTO_TEST_BIO_PREPARE_TIME = 5000;

    public static final String PROPERTY_AUTO_TEST = "sys.goodix.starttest";
    public static final String PROPERTY_TEST_ITME_TIMEOUT = "sys.goodix.timeout";
    public static final String PROPERTY_FINGER_STATUS = "sys.goodix.fingerstatus";
    public static final String PROPERTY_TEST_ORDER = "sys.goodix.testorder";
    public static final String PROPERTY_SWITCH_FINGER_TIME = "sys.goodix.switchfingertime";

    public static final String KEY_AUTO_TEST = "auto_test";

    public static final int FINGERPRINT_ERROR_VENDOR_BASE = 1000;
    public static final int FINGERPRINT_ERROR_ACQUIRED_PARTIAL = 1011;
    public static final int FINGERPRINT_ERROR_ACQUIRED_IMAGER_DIRTY = 1012;
}
