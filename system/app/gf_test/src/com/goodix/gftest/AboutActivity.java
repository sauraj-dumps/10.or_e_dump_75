
package com.goodix.gftest;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import android.widget.Toast;
import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestResultParser;

public class AboutActivity extends Activity {

    private static final String TAG = "AboutActivity";

    private TextView mAlgoVersionView = null;
    private TextView mPreprocessVersionView = null;
    private View mFWVersionLayout = null;
    private TextView mFWVersionView = null;
    private TextView mTAVersionView = null;
    private TextView mTestAppVersionView = null;
    private TextView mVendorIdView = null;
    private View mProductionDateLayout = null;
    private TextView mProductionDateView = null;
    private TextView mCopyRightInfoView = null;
    private int mInternalTestGate = 0;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private GFConfig mConfig = null;
    private Handler mHandler = new Handler();
    private long[] mHits = new long[3];
    private Toast mHidenTestToast = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mGoodixFingerprintManager = new GoodixFingerprintManager(AboutActivity.this);
        mConfig = mGoodixFingerprintManager.getConfig();

        initView();
    }

    public void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mAlgoVersionView = (TextView) findViewById(R.id.algo_version);
        mPreprocessVersionView = (TextView) findViewById(R.id.preprocess_version);

        mFWVersionLayout = findViewById(R.id.fw_version_layout);
        mFWVersionView = (TextView) findViewById(R.id.fw_version);
        if (mConfig != null && mConfig.mChipSeries == Constants.GF_MILAN_F_SERIES) {
            mFWVersionLayout.setVisibility(View.GONE);
        }

        mTAVersionView = (TextView) findViewById(R.id.ta_version);
        mCopyRightInfoView = (TextView) findViewById(R.id.copy_right_info);
        mTestAppVersionView = (TextView) findViewById(R.id.test_app_version);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
            mTestAppVersionView.setText(info.versionName);
        } catch (NameNotFoundException e) {
        }

        mVendorIdView = (TextView) findViewById(R.id.vendor_id);
        mProductionDateLayout = findViewById(R.id.production_date_layout);
        mProductionDateView = (TextView) findViewById(R.id.production_date);

        if (mConfig != null && mConfig.mChipSeries == Constants.GF_MILAN_F_SERIES) {
            mProductionDateLayout.setVisibility(View.GONE);
        }

        mCopyRightInfoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.arraycopy(mHits, 1, mHits, 0, mHits.length - 1);
                mHits[mHits.length - 1] = SystemClock.uptimeMillis();
                if (mInternalTestGate > 0 && mHits[0] >= (SystemClock.uptimeMillis() - 800)) {
                    if (mHidenTestToast != null) {
                        mHidenTestToast.cancel();
                    }

                    mHidenTestToast = Toast
                            .makeText(AboutActivity.this,
                                    getResources().getQuantityString(R.plurals.show_hiden_test,
                                            mInternalTestGate, mInternalTestGate),
                                    Toast.LENGTH_SHORT);
                    mHidenTestToast.show();
                    mInternalTestGate--;
                } else if (mInternalTestGate <= 0) {
                    if (mHidenTestToast != null) {
                        mHidenTestToast.cancel();
                    }

                    Intent intent = new Intent(AboutActivity.this, InternalTestActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mInternalTestGate = 3;
        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_GET_VERSION, null);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {
        @Override
        public void onTestCmd(int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (result == null || cmdId != Constants.CMD_TEST_GET_VERSION) {
                return;
            }
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
                        errorCode = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
                    }

                    String algoVersion = null;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_ALGO_VERSION)) {
                        algoVersion = (String) result
                                .get(TestResultParser.TEST_TOKEN_ALGO_VERSION);
                    }

                    String preprocessVersion = null;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_PREPROCESS_VERSION)) {
                        preprocessVersion = (String) result
                                .get(TestResultParser.TEST_TOKEN_PREPROCESS_VERSION);
                    }

                    String fwVersion = null;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_FW_VERSION)) {
                        fwVersion = (String) result
                                .get(TestResultParser.TEST_TOKEN_FW_VERSION);
                    }

                    String teeVersion = null;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_TEE_VERSION)) {
                        teeVersion = (String) result
                                .get(TestResultParser.TEST_TOKEN_TEE_VERSION);
                    }

                    String taVersion = null;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_TA_VERSION)) {
                        taVersion = (String) result
                                .get(TestResultParser.TEST_TOKEN_TA_VERSION);
                    }

                    byte[] vendorId = null;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_VENDOR_ID)) {
                        vendorId = (byte[]) result
                                .get(TestResultParser.TEST_TOKEN_VENDOR_ID);
                    }

                    byte[] productionDate = null;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_PRODUCTION_DATE)) {
                        productionDate = (byte[]) result
                                .get(TestResultParser.TEST_TOKEN_PRODUCTION_DATE);
                    }

                    if (algoVersion != null) {
                        mAlgoVersionView.setText(algoVersion.trim());
                    }
                    if (preprocessVersion != null) {
                        mPreprocessVersionView.setText(preprocessVersion.trim());
                    }
                    if (fwVersion != null) {
                        mFWVersionView.setText(fwVersion.trim());
                    }
                    if (taVersion != null) {
                        mTAVersionView.setText(taVersion.trim().replaceFirst("_", "\n"));
                    }

                    if (vendorId != null) {
                        int vendor = vendorId[0];
                        if (vendorId[0] < 0) {
                            vendor = 256 + vendorId[0];
                        }
                        switch (vendor) {
                            case 0x05:
                                mVendorIdView.setText(R.string.vendor_ofilm);
                                break;
                            case 0x0A:
                                mVendorIdView.setText(R.string.vendor_akerr);
                                break;
                            case 0x15:
                                mVendorIdView.setText(R.string.vendor_ct);
                                break;
                            case 0xA0:
                                mVendorIdView.setText(R.string.vendor_holitech);
                                break;
                            case 0x1A:
                                mVendorIdView.setText(R.string.vendor_truly);
                                break;
                            case 0x25:
                                mVendorIdView.setText(R.string.vendor_lead);
                                break;
                            default:
                                mVendorIdView.setText(R.string.unknown);
                                break;
                        }
                    }

                    if (productionDate != null) {
                        if (productionDate[0] > 0x00 && productionDate[0] <= 0x99) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("20");
                            sb.append((productionDate[0] & 0xf0) >>> 4);
                            sb.append(productionDate[0] & 0x0f);
                            sb.append("-");
                            sb.append((productionDate[1] & 0xf0) >>> 4);
                            sb.append(productionDate[1] & 0x0f);
                            sb.append("-");
                            sb.append((productionDate[2] & 0xf0) >>> 4);
                            sb.append(productionDate[2] & 0x0f);
                            mProductionDateView.setText(sb.toString());
                        } else if (mConfig.mChipSeries == Constants.GF_MILAN_A_SERIES) {
                            StringBuilder sb = new StringBuilder();
                            int year = (int)productionDate[0]+2016;
                            int month = (int)productionDate[1];
                            int day = (int)productionDate[2];
                            sb.append(year);
                            sb.append("-");
                            sb.append(month);
                            sb.append("-");
                            sb.append(day);
                            mProductionDateView.setText(sb.toString());
                        } else {
                            mProductionDateView.setText(R.string.unknown);
                        }
                    }
                }

            });

        }
    };

}
