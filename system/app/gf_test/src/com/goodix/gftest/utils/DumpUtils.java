
package com.goodix.gftest.utils;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.HashMap;

import com.goodix.fingerprint.utils.TestResultParser;

import android.os.Environment;
import android.util.Log;

public class DumpUtils {
    private static final String TAG = "DumpUtils";

    private static final String GF_DUMP_FINGER_BASE_DIR = "/gf_data/base/finger_base/";
    private static final String GF_DUMP_NAV_BASE_DIR = "/gf_data/base/nav_base/";
    private static final String GF_DUMP_NAV_DIR = "/gf_data/navigation/";
    private static final String GF_DUMP_ENROLL_DIR = "/gf_data/enroll/";
    private static final String GF_DUMP_AUTHENTICATE_DIR = "/gf_data/authenticate/";
    private static final String GF_DUMP_TEST_PIXEL_OPEN_DIR = "/gf_data/test_sensor/";
    private static final String GF_DUMP_CONSISTENCY_TEST_DIR = "/gf_data/consistency/";
    private static final String GF_DUMP_TEST_UNTRUSTED_ENROLL_DIR = "/gf_data/untrusted_enroll/";
    private static final String GF_DUMP_TEST_UNTRUSTED_AUTHENTICATE_DIR = "/gf_data/untrusted_authenticate/";
    private static final String GF_DUMP_GSC_DIR = "/gf_data/gsc/";
    private static final String GF_DUMP_TEMPLATE_DIR = "/gf_data/templates/";
    private static final String GF_DUMP_ENCRYPTED_DATA_DIR = "/gf_data/encrypted_data/";
    private static final String GF_DUMP_ENCRYPTED_TEMPLATE_DIR = "/gf_data/encrypted_templates/";

    private static final int OPERATION_ENROLL = 0;
    private static final int OPERATION_AUTHENTICATE_IMAGE = 1;
    private static final int OPERATION_AUTHENTICATE_FF = 2;
    private static final int OPERATION_AUTHENTICATE_SLEEP = 3;
    private static final int OPERATION_AUTHENTICATE_FIDO = 4;
    private static final int OPERATION_FINGER_BASE = 5;
    private static final int OPERATION_NAV = 6;
    private static final int OPERATION_NAV_BASE = 7;
    private static final int OPERATION_HEARTBEAT_KEY = 12;
    private static final int OPERATION_TEST_PIXEL_OPEN_STEP1 = 24;
    private static final int OPERATION_TEST_PIXEL_OPEN_STEP2 = 25;
    private static final int OPERATION_TEST_BAD_POINT =26;
    private static final int OPERATION_TEST_UNTRUSTED_ENROLL = 33;
    private static final int OPERATION_TEST_UNTRUSTED_AUTHENTICATE = 34;
    private static final int OPERATION_TEST_BIO_CALIBRATION = 36;
    private static final int OPERATION_TEST_HBD_CALIBRATION = 37;

    private static final int GF_SUCCESS = 0;
    private static final int GF_ERROR_SENSOR_IS_BROKEN = 1062;
    private static final int GF_ERROR_INVALID_BASE = 1059;
    private static final int GF_ERROR_INVALID_FINGER_PRESS = 1053;

    private static long getUnsignedInt(int value) {
        return value & 0x0FFFFFFFFL;
    }

    private static int getUnsignedShort(int value) {
        return value & 0x0FFFF;
    }

    private static void dumpDatFile(String filePath, byte[] data) {
        RandomAccessFile file = null;

        try {
            file = new RandomAccessFile(filePath, "rw");

            file.write(data);
        } catch (IOException e) {
        }

        if (file != null) {
            try {
                file.close();
            } catch (IOException e) {
            }
        }
    }

    public static void dumpTemplate(int fingerId, byte[] data, long timestamp) {
        Log.d(TAG, "dumpTemplate");
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        StringBuilder filePath = new StringBuilder();
        filePath.append(Environment.getExternalStorageDirectory().getPath());
        filePath.append(GF_DUMP_TEMPLATE_DIR);
        filePath.append("finger_");
        filePath.append(getUnsignedInt(fingerId));
        filePath.append("_");
        filePath.append(timestamp);
        filePath.append(".dat");

        File file = new File(filePath.toString());
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        dumpDatFile(filePath.toString(), data);
    }

    public static void dumpEncryptedTemplate(byte[] data, long timestamp) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        StringBuilder filePath = new StringBuilder();
        filePath.append(Environment.getExternalStorageDirectory().getPath());
        filePath.append(GF_DUMP_ENCRYPTED_TEMPLATE_DIR);
        filePath.append(timestamp);
        filePath.append(".dat");

        File file = new File(filePath.toString());
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        dumpDatFile(filePath.toString(), data);
    }

    public static void dumpEncryptedData(byte[] data, long timestamp) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        StringBuilder filePath = new StringBuilder();
        filePath.append(Environment.getExternalStorageDirectory().getPath());
        filePath.append(GF_DUMP_ENCRYPTED_DATA_DIR);
        filePath.append(timestamp);
        filePath.append(".dat");

        File file = new File(filePath.toString());
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        dumpDatFile(filePath.toString(), data);
    }

    private static void dumpRawDataToCsvFile(String filePath, byte[] rawData, int width,
            int height) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        File file = new File(filePath);
        BufferedWriter bufferedWriter = null;

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        try {
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new FileWriter(file));

            int value = 0;
            int offset = 0;
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    value = ((rawData[offset + 1] & 0x0FF) << 8) | (rawData[offset] & 0x0FF);

                    bufferedWriter.write(String.format("%4d,", value));
                    offset += 2;
                }
                bufferedWriter.newLine();
            }

            bufferedWriter.flush();

        } catch (IOException e) {
        }

        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
            }
        }
    }

    private static void dumpDataToCsvFile(String filePath, byte[] data, int width, int height) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        BufferedWriter bufferedWriter = null;
        File file = new File(filePath);

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        try {
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new FileWriter(file));

            int value = 0;
            int offset = 0;
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    value = (data[offset] & 0x0FF);

                    bufferedWriter.write(String.format("%d,", value));
                    offset++;
                }
                bufferedWriter.newLine();
            }

            bufferedWriter.flush();

        } catch (IOException e) {
        }

        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
            }
        }

    }

    private static void dumpBaseInfo(String filePath, HashMap<Integer, Object> data) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        BufferedWriter bufferedWriter = null;
        File file = new File(filePath);

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        try {
            file.createNewFile();
            bufferedWriter = new BufferedWriter(new FileWriter(file));

            if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_PREPROCESS_VERSION)) {
                bufferedWriter.write("preprocess version, ");
                String preprocessVersion = (String) data
                        .get(TestResultParser.TEST_TOKEN_DUMP_PREPROCESS_VERSION);
                bufferedWriter
                        .write(preprocessVersion.substring(0, preprocessVersion.indexOf(0x00)));
                bufferedWriter.write("\n");
            }

            // sensor id
            if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_SENSOR_ID)) {
                byte sensorID[] = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_SENSOR_ID);
                if (sensorID != null && sensorID.length > 0) {
                    bufferedWriter.write("sensor id, ");
                    for (int i = 0; i < sensorID.length; i++) {
                        bufferedWriter.write(String.format("0x%02X", sensorID[i]));
                        bufferedWriter.write(", ");
                    }
                    bufferedWriter.write("\n");
                }
            }

            // chip id
            if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_CHIP_ID)) {
                byte chipId[] = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_CHIP_ID);
                if (chipId != null && chipId.length > 0) {
                    bufferedWriter.write("chip id, ");
                    for (int i = 0; i < chipId.length; i++) {
                        bufferedWriter.write(String.format("0x%02X", chipId[i]));
                        bufferedWriter.write(", ");
                    }
                    bufferedWriter.write("\n");
                }
            }

            // vendor id
            if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_VENDOR_ID)) {
                byte vendorId[] = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_VENDOR_ID);
                bufferedWriter.write("vendor id, ");
                if (vendorId != null && vendorId.length > 0) {
                    for (int i = 0; i < vendorId.length; i++) {
                        bufferedWriter.write(String.format("0x%02X", vendorId[i]));
                        bufferedWriter.write(", ");
                    }
                    bufferedWriter.write("\n");
                }
            }

            if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_FRAME_NUM)) {
                int frameNum = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_FRAME_NUM);
                bufferedWriter.write("frame num, ");
                bufferedWriter.write(String.format("%d", frameNum));
            }

            bufferedWriter.flush();

        } catch (IOException e) {
        }

        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
            }
        }
    }

    private static void dumpImageToBmpFile(String filePath, byte[] image, int width, int height) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        File file = new File(filePath);
        DataOutputStream outputStream = null;

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        try {
            file.createNewFile();

            outputStream = new DataOutputStream(new FileOutputStream(file));
            int lineWidth = (width + 3) / 4 * 4;
            int offBits = 1078;
            int size = offBits + lineWidth * height;

            outputStream.writeShort(0x424d);
            outputStream.writeInt(Integer.reverseBytes(size));
            outputStream.writeShort(0);
            outputStream.writeShort(0);
            outputStream.writeInt(Integer.reverseBytes(offBits));

            int infoSize = 40;
            int infoWidth = width;
            int infoHeight = height;
            int infoSizeImage = lineWidth * height;
            outputStream.writeInt(Integer.reverseBytes(infoSize));
            outputStream.writeInt(Integer.reverseBytes(infoWidth));
            outputStream.writeInt(Integer.reverseBytes(infoHeight));
            outputStream.writeShort(Short.reverseBytes((short) 1));
            outputStream.writeShort(Short.reverseBytes((short) 8));
            outputStream.writeInt(0);
            outputStream.writeInt(Integer.reverseBytes(infoSizeImage));
            outputStream.writeInt(0);
            outputStream.writeInt(0);
            outputStream.writeInt(0);
            outputStream.writeInt(0);

            int alpha = 0;
            for (int i = 0; i < 256; i++) {
                outputStream.writeByte(i);
                outputStream.writeByte(i);
                outputStream.writeByte(i);
                outputStream.writeByte(alpha);
            }

            for (int i = height - 1; i >= 0; i--) {
                outputStream.write(image, i * width, width);
                if (lineWidth > width) {
                    for (int j = 0; j < lineWidth - width; j++) {
                        outputStream.writeByte(0);
                    }
                }
            }

            outputStream.flush();
        } catch (IOException e) {
        }

        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (IOException e) {
            }
        }
    }

    private static void dumpCalibrationParams(HashMap<Integer, Object> data, String resultStr,
            String dir, String curTime, int width, int height) {
        StringBuilder filePath = null;

        // dump raw data to ".csv" file
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA)) {
            filePath = new StringBuilder();
            filePath.append(dir);
            filePath.append(curTime);
            filePath.append("_");
            filePath.append(resultStr);
            filePath.append("_rawdata.csv");
            dumpRawDataToCsvFile(filePath.toString(),
                    (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA),
                    width, height);
        }

        // dump kr to ".csv" file
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_KR)) {
            filePath = new StringBuilder();
            filePath.append(dir);
            filePath.append(curTime);
            filePath.append("_kr.csv");
            dumpRawDataToCsvFile(filePath.toString(),
                    (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_KR),
                    width, height);
        }

        // dump b to ".csv" file
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_B)) {
            filePath = new StringBuilder();
            filePath.append(dir);
            filePath.append(curTime);
            filePath.append("_b.csv");
            dumpRawDataToCsvFile(filePath.toString(),
                    (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_B),
                    width, height);
        }

        // dump basic information for base frame
        filePath = new StringBuilder();
        filePath.append(dir);
        filePath.append(curTime);
        filePath.append("_base_info.csv");
        dumpBaseInfo(filePath.toString(), data);
    }

    private static void dumpNavData(HashMap<Integer, Object> data) {
        if (null == data) {
            return;
        }

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        int errorCode = GF_SUCCESS;
        int operation = 0;
        int width = 0;
        int height = 0;
        int navFrameCount = 0;

        if (data.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
            errorCode = (Integer) data.get(TestResultParser.TEST_TOKEN_ERROR_CODE);
        }

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_OPERATION)) {
            operation = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_OPERATION);
        }

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_WIDTH)) {
            width = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_WIDTH);
        }

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_HEIGHT)) {
            height = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_HEIGHT);
        }

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_NAV_FRAME_COUNT)) {
            navFrameCount = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_NAV_FRAME_COUNT);
        }

        StringBuilder curTimeStr = new StringBuilder();
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_YEAR)) {
            curTimeStr.append(1900 + (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_YEAR));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_MONTH)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_MONTH) + 1));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_DAY)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_DAY)));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_HOUR)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_HOUR)));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_MINUTE)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_MINUTE)));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_SECOND)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_SECOND)));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_MICROSECOND)) {
            curTimeStr.append(String.format("%06d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_MICROSECOND)));
        }

        StringBuilder filePath = null;
        switch (operation) {

            case OPERATION_NAV_BASE: {
                byte[] rawData = null;
                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA)) {
                    filePath = new StringBuilder();
                    filePath.append(Environment.getExternalStorageDirectory().getPath());
                    filePath.append(GF_DUMP_NAV_BASE_DIR);
                    filePath.append(curTimeStr);
                    filePath.append("_rawdata.csv");

                    rawData = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA);
                    dumpRawDataToCsvFile(filePath.toString(), rawData, width, height);
                }
                break;
            }

            case OPERATION_NAV: {
                byte[] rawData = null;
                int navTimes = 0;
                int navFrameIndex = 0;

                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA)) {
                    filePath = new StringBuilder();
                    filePath.append(Environment.getExternalStorageDirectory().getPath());
                    filePath.append(GF_DUMP_NAV_DIR);
                    // filePath.append(curTimeStr);
                    // filePath.append("_");
                    filePath.append("navigation_rawdata_");

                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_NAV_TIMES)) {
                        navTimes = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_NAV_TIMES);
                    }

                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_NAV_FRAME_INDEX)) {
                        navFrameIndex = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_NAV_FRAME_INDEX);
                    }

                    rawData = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA);

                    StringBuilder tmpPath = new StringBuilder();
                    tmpPath.append(filePath);
                    for (int i = 0; i < navFrameCount; i++) {
                        byte[] navFrameNum = (byte[]) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_NAV_FRAME_NUM);
                        for (int j = 0; j < navFrameNum[i]; j++) {
                            filePath = new StringBuilder();
                            filePath.append(tmpPath);
                            filePath.append(String.format("%d_", navTimes));
                            filePath.append(String.format("%02d", navFrameIndex));
                            filePath.append(".csv");

                            dumpRawDataToCsvFile(filePath.toString(),
                                    Arrays.copyOfRange(rawData, i * rawData.length / 20,
                                            i * rawData.length / 20 + 2 * (j + 1) * width * height),
                                    width, height);
                            navFrameIndex++;
                        }

                    }
                }
                break;
            }

            default: {
                break;
            }
        }
    }

    private static void dumpImageData(HashMap<Integer, Object> data) {
        if (null == data) {
            return;
        }

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        int errorCode = GF_SUCCESS;
        int operation = 0;
        int width = 0;
        int height = 0;
        int frameNum = 0;

        if (data.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
            errorCode = (Integer) data.get(TestResultParser.TEST_TOKEN_ERROR_CODE);
        }

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_OPERATION)) {
            operation = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_OPERATION);
        }

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_WIDTH)) {
            width = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_WIDTH);
        }

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_HEIGHT)) {
            height = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_HEIGHT);
        }

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_FRAME_NUM)) {
            frameNum = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_FRAME_NUM);
        }

        String resultStr = null;
        switch (errorCode) {
            case GF_SUCCESS:
                resultStr = "success";
                break;

            case GF_ERROR_SENSOR_IS_BROKEN:
                resultStr = "sensor_is_damaged";
                break;

            case GF_ERROR_INVALID_BASE:
                resultStr = "bad_base";
                break;

            case GF_ERROR_INVALID_FINGER_PRESS:
                resultStr = "invalid_finger_press";
                break;

            default:
                resultStr = "fail";
                break;
        }

        StringBuilder curTimeStr = new StringBuilder();
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_YEAR)) {
            curTimeStr.append(1900 + (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_YEAR));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_MONTH)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_MONTH) + 1));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_DAY)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_DAY)));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_HOUR)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_HOUR)));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_MINUTE)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_MINUTE)));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_SECOND)) {
            curTimeStr.append(String.format("%02d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_SECOND)));
            curTimeStr.append("-");
        }
        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_MICROSECOND)) {
            curTimeStr.append(String.format("%06d",
                    (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_MICROSECOND)));
        }

        StringBuilder dirPath = new StringBuilder();
        StringBuilder filePath = null;
        switch (operation) {
            case OPERATION_ENROLL:
            case OPERATION_AUTHENTICATE_IMAGE:
            case OPERATION_AUTHENTICATE_FF:
            case OPERATION_AUTHENTICATE_SLEEP:
            case OPERATION_AUTHENTICATE_FIDO:
            case OPERATION_TEST_UNTRUSTED_ENROLL:
            case OPERATION_TEST_UNTRUSTED_AUTHENTICATE: {
                dirPath.append(Environment.getExternalStorageDirectory().getPath());

                if (OPERATION_ENROLL == operation) {
                    dirPath.append(GF_DUMP_ENROLL_DIR);
                    int fingerId = 0;
                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_ENROLLING_FINGER_ID)) {
                        fingerId = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_ENROLLING_FINGER_ID);
                    }
                    dirPath.append(getUnsignedInt(fingerId));
                    dirPath.append("/");
                } else if (OPERATION_TEST_UNTRUSTED_ENROLL == operation) {
                    dirPath.append(GF_DUMP_TEST_UNTRUSTED_ENROLL_DIR);
                    int fingerId = 0;
                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_ENROLLING_FINGER_ID)) {
                        fingerId = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_ENROLLING_FINGER_ID);
                    }
                    dirPath.append(getUnsignedInt(fingerId));
                    dirPath.append("/");
                } else if (OPERATION_TEST_UNTRUSTED_ENROLL == operation) {
                    dirPath.append(GF_DUMP_TEST_UNTRUSTED_AUTHENTICATE_DIR);
                } else {
                    dirPath.append(GF_DUMP_AUTHENTICATE_DIR);
                }

                int sensorBrokenCheckFrameNum = 0;
                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_BROKEN_CHECK_FRAME_NUM)) {
                    sensorBrokenCheckFrameNum = (Integer) data
                            .get(TestResultParser.TEST_TOKEN_DUMP_BROKEN_CHECK_FRAME_NUM);
                }
                for (int i = 0; i < sensorBrokenCheckFrameNum; i++) {
                    filePath = new StringBuilder();
                    filePath.append(dirPath);
                    filePath.append(curTimeStr);
                    filePath.append("_");
                    filePath.append(resultStr);
                    filePath.append("_sensor_broken_");
                    filePath.append(i);
                    filePath.append(".csv");

                    byte rawData[] = (byte[]) data.get(
                            TestResultParser.TEST_TOKEN_DUMP_BROKEN_CHECK_RAW_DATA);
                    if (rawData == null) {
                        break;
                    }

                    dumpRawDataToCsvFile(filePath.toString(),
                            Arrays.copyOfRange(rawData, i * rawData.length / 2,
                                    (i + 1) * rawData.length / 2),
                            width, height);
                }

                dumpCalibrationParams(data, resultStr, dirPath.toString(), curTimeStr.toString(),
                        width, height);

                // dump caliRes
                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_CALI_RES)) {
                    filePath = new StringBuilder();
                    filePath.append(dirPath);
                    filePath.append(curTimeStr);
                    filePath.append("_");
                    filePath.append(resultStr);
                    filePath.append("_calires.csv");
                    byte[] caliResData = (byte[]) data
                            .get(TestResultParser.TEST_TOKEN_DUMP_CALI_RES);
                    dumpRawDataToCsvFile(filePath.toString(), caliResData, width, height);
                }

                // dump dataBmp
                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_DATA_BMP)) {
                    byte[] bmpData = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_DATA_BMP);

                    filePath = new StringBuilder();
                    filePath.append(dirPath);
                    filePath.append(curTimeStr);
                    filePath.append("_");
                    filePath.append(resultStr);
                    filePath.append("_databmp.csv");

                    dumpDataToCsvFile(filePath.toString(), bmpData, width, height);

                    filePath = new StringBuilder();
                    filePath.append(dirPath);
                    filePath.append(curTimeStr);
                    filePath.append("_");
                    filePath.append(resultStr);
                    filePath.append("_databmp.bmp");

                    dumpImageToBmpFile(filePath.toString(), bmpData, width, height);
                }

                // dump sitoBmp
                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_SITO_BMP)) {
                    byte[] sitoData = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_SITO_BMP);

                    filePath = new StringBuilder();
                    filePath.append(dirPath);
                    filePath.append(curTimeStr);
                    filePath.append("_");
                    filePath.append(resultStr);
                    filePath.append("_sitobmp.csv");

                    dumpDataToCsvFile(filePath.toString(), sitoData, width, height);

                    filePath = new StringBuilder();
                    filePath.append(dirPath);
                    filePath.append(curTimeStr);
                    filePath.append("_");
                    filePath.append(resultStr);
                    filePath.append("_sitobmp.bmp");

                    dumpImageToBmpFile(filePath.toString(), sitoData, width, height);
                }

                // dump select bmp
                int selectIndex = 0;
                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_SELECT_INDEX)) {
                    selectIndex = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_SELECT_INDEX);

                    byte[] selectBmp = null;
                    int imageQuality = 0;
                    int imageValidArea = 0;
                    int overlapRateToLastTemp = 0;
                    int overlapRateToBigTemp = 0;
                    int duplicatedFingerID = 0;
                    int matchScore = 0;
                    int matchFingerID = 0;
                    int studyFlag = 0;

                    if (selectIndex == 0) {
                        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_DATA_BMP)) {
                            selectBmp = (byte[]) data
                                    .get(TestResultParser.TEST_TOKEN_DUMP_DATA_BMP);
                        }
                    } else {
                        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_SITO_BMP)) {
                            selectBmp = (byte[]) data
                                    .get(TestResultParser.TEST_TOKEN_DUMP_SITO_BMP);
                        }
                    }

                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_IMAGE_QUALITY)) {
                        imageQuality = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_IMAGE_QUALITY);
                    }

                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_VALID_AREA)) {
                        imageValidArea = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_VALID_AREA);
                    }

                    if (data.containsKey(
                            TestResultParser.TEST_TOKEN_DUMP_OVERLAP_RATE_BETWEEN_LAST_TEMPLATE)) {
                        overlapRateToLastTemp = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_OVERLAP_RATE_BETWEEN_LAST_TEMPLATE);
                    }

                    if (data.containsKey(
                            TestResultParser.TEST_TOKEN_DUMP_INCREASE_RATE_BETWEEN_STITCH_INFO)) {
                        overlapRateToBigTemp = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_INCREASE_RATE_BETWEEN_STITCH_INFO);
                    }

                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_DUMPLICATED_FINGER_ID)) {
                        duplicatedFingerID = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_DUMPLICATED_FINGER_ID);
                    }

                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_MATCH_SCORE)) {
                        matchScore = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_MATCH_SCORE);
                    }

                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_MATCH_FINGER_ID)) {
                        matchFingerID = (Integer) data
                                .get(TestResultParser.TEST_TOKEN_DUMP_MATCH_FINGER_ID);
                    }

                    if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_STUDY_FLAG)) {
                        studyFlag = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_STUDY_FLAG);
                    }

                    if (OPERATION_ENROLL == operation) {
                        filePath = new StringBuilder();
                        filePath.append(dirPath);
                        filePath.append(curTimeStr);
                        filePath.append("_");
                        filePath.append(resultStr);
                        filePath.append("_selectbmp_");
                        filePath.append(imageQuality);
                        filePath.append("_");
                        filePath.append(imageValidArea);
                        filePath.append("_");
                        filePath.append(overlapRateToLastTemp);
                        filePath.append("_");
                        filePath.append(overlapRateToBigTemp);
                        filePath.append("_");
                        filePath.append(getUnsignedInt(duplicatedFingerID));
                        filePath.append(".bmp");

                        dumpImageToBmpFile(filePath.toString(), selectBmp, width, height);
                        dumpDataToCsvFile(filePath.replace(filePath.lastIndexOf(".bmp"),
                                filePath.length(), ".csv").toString(), selectBmp, width, height);
                    } else {
                        filePath = new StringBuilder();
                        filePath.append(dirPath);
                        filePath.append(curTimeStr);
                        filePath.append("_");
                        filePath.append(resultStr);
                        filePath.append("_selectbmp_");
                        filePath.append(imageQuality);
                        filePath.append("_");
                        filePath.append(imageValidArea);
                        filePath.append("_");
                        filePath.append(matchScore);
                        filePath.append("_");
                        filePath.append(getUnsignedInt(matchFingerID));
                        filePath.append("_");
                        filePath.append(studyFlag);
                        filePath.append(".bmp");

                        dumpImageToBmpFile(filePath.toString(), selectBmp, width, height);
                        dumpDataToCsvFile(filePath.replace(filePath.lastIndexOf(".bmp"),
                                filePath.length(), ".csv").toString(), selectBmp, width, height);
                    }
                }
                break;
            }

            case OPERATION_FINGER_BASE: {
                dirPath.append(Environment.getExternalStorageDirectory().getPath());
                dirPath.append(GF_DUMP_FINGER_BASE_DIR);

                dumpCalibrationParams(data, "success", dirPath.toString(), curTimeStr.toString(),
                        width, height);
                break;
            }

            case OPERATION_TEST_PIXEL_OPEN_STEP1:
            case OPERATION_TEST_PIXEL_OPEN_STEP2: {
                filePath = new StringBuilder();
                filePath.append(Environment.getExternalStorageDirectory().getPath());
                filePath.append(GF_DUMP_TEST_PIXEL_OPEN_DIR);
                filePath.append(curTimeStr);

                if (OPERATION_TEST_PIXEL_OPEN_STEP1 == operation) {
                    filePath.append("_a");
                } else {
                    filePath.append("_b");
                }
                filePath.append(".csv");
                byte[] rawData = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA);
                dumpRawDataToCsvFile(filePath.toString(), rawData, width, height);
                break;
            }

            case OPERATION_TEST_BAD_POINT: {
                byte[] rawData = null;
                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA)) {
                    filePath = new StringBuilder();
                    filePath.append(Environment.getExternalStorageDirectory().getPath());
                    filePath.append(GF_DUMP_CONSISTENCY_TEST_DIR);
                    filePath.append(curTimeStr);
                    filePath.append("_");
                    filePath.append(frameNum);
                    filePath.append("_rawdata.csv");

                    rawData = (byte[]) data.get(TestResultParser.TEST_TOKEN_DUMP_RAW_DATA);
                    dumpRawDataToCsvFile(filePath.toString(), rawData, width, height);
                }
                break;
            }

            default: {
                break;
            }
        }
    }

    public static void dumpData(HashMap<Integer, Object> data) {
        if (null == data) {
            return;
        }

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }

        int operation = 0;

        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_OPERATION)) {
            operation = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_OPERATION);
        }

        switch (operation) {
            case OPERATION_NAV:
            case OPERATION_NAV_BASE:
                dumpNavData(data);
                break;

            case OPERATION_HEARTBEAT_KEY:
            case OPERATION_TEST_BIO_CALIBRATION:
            case OPERATION_TEST_HBD_CALIBRATION:
                // TODO: dump hbd data
                break;

            default:
                dumpImageData(data);
                break;
        }

    }
}
