
package com.goodix.gftest;

import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.utils.TestResultChecker.Checker;
import com.goodix.gftest.utils.TestResultChecker.TestResultCheckerFactory;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SpiTestActivity extends Activity {

    private static final String TAG = "SpiTestActivity";

    private TextView mCondition = null;
    private TextView mResultView = null;
    private View mResultDetailView = null;
    private TextView mFwVersionView = null;
    private ProgressBar mProgressBar = null;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private GFConfig mConfig = null;
    private Handler mHandler = new Handler();
    private boolean mIsCanceled = false;
    private Checker mTestChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spi_test);

        mGoodixFingerprintManager = new GoodixFingerprintManager(SpiTestActivity.this);
        mConfig = mGoodixFingerprintManager.getConfig();
        mTestChecker = TestResultCheckerFactory.getInstance(mConfig.mChipType, mConfig.mChipSeries);

        initView();
    }

    public void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mResultView = (TextView) findViewById(R.id.test_result);
        mResultDetailView = findViewById(R.id.test_result_detail);
        mFwVersionView = (TextView) findViewById(R.id.test_spi_result);
        mProgressBar = (ProgressBar) findViewById(R.id.testing);
        mResultView.setVisibility(View.INVISIBLE);
        mResultDetailView.setVisibility(View.INVISIBLE);

        mCondition = (TextView) findViewById(R.id.test_spi_condition);
        if (mConfig != null && mConfig.mChipSeries == Constants.GF_MILAN_F_SERIES) {
            mCondition.setText(R.string.test_spi_chip_id);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SPI, null);
        mIsCanceled = false;
        mHandler.postDelayed(mTimeoutRunnable, Constants.TEST_TIMEOUT_MS);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mHandler.removeCallbacks(mTimeoutRunnable);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PRIOR_CANCEL, null);
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Runnable mTimeoutRunnable = new Runnable() {

        @Override
        public void run() {
            mIsCanceled = true;
            mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PRIOR_CANCEL, null);

            mResultView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            mResultView.setText(R.string.timeout);
            mResultView.setTextColor(getResources().getColor(R.color.test_failed_color));
        }

    };

    private void onTestSpi(final HashMap<Integer, Object> result) {

        mFwVersionView.setVisibility(View.VISIBLE);
        mResultView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
        mResultDetailView.setVisibility(View.VISIBLE);

        int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
        int chipID = 0;
        String fwVersion = null;
        int sensorOtpType = 0;
        if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
            errorCode = (Integer) result
                    .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
        }

        if (result.containsKey(TestResultParser.TEST_TOKEN_FW_VERSION)) {
            fwVersion = (String) result
                    .get(TestResultParser.TEST_TOKEN_FW_VERSION);
            mFwVersionView.setText(fwVersion);
        }

        if (result.containsKey(TestResultParser.TEST_TOKEN_CHIP_ID)) {
            byte[] chip = (byte[]) result.get(TestResultParser.TEST_TOKEN_CHIP_ID);
            if (chip != null && chip.length >= 4) {
                chipID = TestResultParser.decodeInt32(chip, 0) >> 8;
            }

            mFwVersionView.setText("0x" + Integer.toHexString(chipID));
        }
        if (result.containsKey(TestResultParser.TEST_TOKEN_SENSOR_OTP_TYPE)) {
            sensorOtpType = Integer.valueOf(result.get(TestResultParser.TEST_TOKEN_SENSOR_OTP_TYPE).toString());
        }

        if (mTestChecker.checkSpiTestResult(errorCode, fwVersion, chipID, sensorOtpType)) {
            mResultView.setText(R.string.test_succeed);
            mResultView.setTextColor(getResources()
                    .getColor(R.color.test_succeed_color));
        } else {
            mResultView.setText(R.string.test_failed);
            mResultView.setTextColor(getResources()
                    .getColor(R.color.test_failed_color));
        }

    }

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {

        @Override
        public void onTestCmd(int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (result == null || cmdId != Constants.CMD_TEST_SPI) {
                return;
            }

            mHandler.removeCallbacks(mTimeoutRunnable);
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mIsCanceled) {
                        return;
                    }
                    onTestSpi(result);
                }

            });

        }

    };
}
