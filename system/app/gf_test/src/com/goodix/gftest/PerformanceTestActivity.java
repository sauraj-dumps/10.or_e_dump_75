
package com.goodix.gftest;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.LinearLayout;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.R;

public class PerformanceTestActivity extends Activity {

    private static final String TAG = "PerformanceTestActivity";

    private ArrayList<PerformanceTestItem> mData = new ArrayList<PerformanceTestItem>();
    private ListView mListView = null;
    private TestAdapter mAdapter = null;
    private boolean mIsCanceled = false;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private Handler mHandler = new Handler();
    private GFConfig mConfig = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance_test);

        mGoodixFingerprintManager = new GoodixFingerprintManager(PerformanceTestActivity.this);
        if (null != mGoodixFingerprintManager) {
            mConfig = mGoodixFingerprintManager.getConfig();
        }

        initView();
    }

    public void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mListView = (ListView) findViewById(R.id.listview);
        mAdapter = new TestAdapter();
        mListView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PERFORMANCE, null);

        mIsCanceled = false;
        mHandler.postDelayed(mTimeoutRunnable, Constants.TEST_TIMEOUT_MS);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mTimeoutRunnable);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class TestAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mData != null ? mData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mData != null ? mData.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(PerformanceTestActivity.this).inflate(
                        R.layout.item_performance_test, null);
                holder = new Holder();
                holder.imageQualityView = (TextView) convertView.findViewById(R.id.image_quality);
                holder.validAreaView = (TextView) convertView.findViewById(R.id.valid_area);
                holder.keyPointNumView = (TextView) convertView.findViewById(R.id.key_point_num);
                holder.increaseRateView = (TextView) convertView.findViewById(R.id.increase_rate);
                holder.overlayView = (TextView) convertView.findViewById(R.id.overlay);
                holder.getRawDataTimeView = (TextView) convertView
                        .findViewById(R.id.get_raw_data_time);
                holder.preprocessTimeView = (TextView) convertView
                        .findViewById(R.id.preprocess_time);
                holder.getFeatureTimeView = (TextView) convertView
                        .findViewById(R.id.get_feature_time);
                holder.enrollTimeView = (TextView) convertView.findViewById(R.id.enroll_time);
                holder.authenticateTimeView = (TextView) convertView
                        .findViewById(R.id.authenticate_time);
                if (null != mConfig && (mConfig.mChipType == Constants.GF_CHIP_5206
                    || mConfig.mChipType == Constants.GF_CHIP_5208)) {
                    holder.gscContainer = (LinearLayout) convertView
                            .findViewById(R.id.gsc_container);
                    holder.getGscTime = (TextView) convertView.findViewById(R.id.gsc_time);
                    holder.bioAssayContainer = (LinearLayout) convertView
                            .findViewById(R.id.bio_assay_container);
                    holder.bioAssayTime = (TextView) convertView.findViewById(R.id.bio_assay_time);
                }
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            PerformanceTestItem item = mData.get(position);
            holder.imageQualityView.setText(String.valueOf(item.imageQuality));
            holder.validAreaView.setText(String.valueOf(item.validArea));
            holder.keyPointNumView.setText(String.valueOf(item.keyPointNum));
            holder.increaseRateView.setText(String.valueOf(item.increaseRate) + "%");
            holder.overlayView.setText(String.valueOf(item.overlay) + "%");
            holder.getRawDataTimeView.setText(item.getRawDataTimeString);
            holder.preprocessTimeView.setText(item.preprocessTimeString);
            holder.getFeatureTimeView.setText(item.getFeatureTimeString);
            holder.enrollTimeView.setText(item.enrollTimeString);
            holder.authenticateTimeView.setText(item.authenticateTimeString);

            if (null != mConfig && (mConfig.mChipType == Constants.GF_CHIP_5206
                || mConfig.mChipType == Constants.GF_CHIP_5208)) {
                holder.gscContainer.setVisibility(View.VISIBLE);
                holder.getGscTime.setText(item.getGscDataTimeString);
                holder.bioAssayContainer.setVisibility(View.VISIBLE);
                holder.bioAssayTime.setText(item.bioAssayTimeString);
            }

            return convertView;
        }

        private class Holder {
            TextView imageQualityView;
            TextView validAreaView;
            TextView keyPointNumView;
            TextView increaseRateView;
            TextView overlayView;
            TextView getRawDataTimeView;
            TextView preprocessTimeView;
            TextView getFeatureTimeView;
            TextView enrollTimeView;
            TextView authenticateTimeView;
            LinearLayout gscContainer;
            TextView getGscTime;
            LinearLayout bioAssayContainer;
            TextView bioAssayTime;
        }
    }

    private String getTimeInMillisecond(int time) {
        StringBuilder sb = new StringBuilder();
        if (0 == time) {
            sb.append(getResources().getString(R.string.less_than_ms_time));
        } else {
            sb.append(String.valueOf(time));
        }
        sb.append("ms");
        return sb.toString();
    }

    private class PerformanceTestItem {
        int imageQuality;
        int validArea;
        int keyPointNum;
        int increaseRate;
        int overlay;
        int getRawDataTime;
        int preprocessTime;
        int getFeatureTime;
        int enrollTime;
        int authenticateTime;
        int getGscDataTime;
        int bioAssayTime;
        String getRawDataTimeString;
        String preprocessTimeString;
        String getFeatureTimeString;
        String enrollTimeString;
        String authenticateTimeString;
        String getGscDataTimeString;
        String bioAssayTimeString;
    }

    private void showTimeoutUI() {

        AlertDialog timeoutDialog = new AlertDialog.Builder(this)
            .setMessage(PerformanceTestActivity.this.getString(R.string.timeout_before_press, Constants.TEST_TIMEOUT_MS / 1000))
            .setTitle(PerformanceTestActivity.this.getString(R.string.sytem_info))
            .setPositiveButton(PerformanceTestActivity.this.getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PerformanceTestActivity.this.finish();
                }
            }).create();
        timeoutDialog.setCancelable(false);
        timeoutDialog.show();
    }

    private Runnable mTimeoutRunnable = new Runnable() {

        @Override
        public void run() {
            mIsCanceled = true;
            mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
            showTimeoutUI();
            Log.e(TAG, "time out,performance test is canceled.");
        }

    };

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {
        @Override
        public void onTestCmd(int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (result == null || cmdId != Constants.CMD_TEST_PERFORMANCE) {
                return;
            }
            mHandler.removeCallbacks(mTimeoutRunnable);
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mIsCanceled) {
                        return;
                    }
                    int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
                        errorCode = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
                    }
                    PerformanceTestItem item = new PerformanceTestItem();
                    if (result.containsKey(TestResultParser.TEST_TOKEN_IMAGE_QUALITY)) {
                        item.imageQuality = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_IMAGE_QUALITY);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_VALID_AREA)) {
                        item.validArea = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_VALID_AREA);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_KEY_POINT_NUM)) {
                        item.keyPointNum = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_KEY_POINT_NUM);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_INCREATE_RATE)) {
                        item.increaseRate = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_INCREATE_RATE);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_OVERLAY)) {
                        item.overlay = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_OVERLAY);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_GET_RAW_DATA_TIME)) {
                        item.getRawDataTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_GET_RAW_DATA_TIME);
                        item.getRawDataTimeString = getTimeInMillisecond(item.getRawDataTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_PREPROCESS_TIME)) {
                        item.preprocessTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_PREPROCESS_TIME);
                        item.preprocessTimeString = getTimeInMillisecond(item.preprocessTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_GET_FEATURE_TIME)) {
                        item.getFeatureTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_GET_FEATURE_TIME);
                        item.getFeatureTimeString = getTimeInMillisecond(item.getFeatureTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_ENROLL_TIME)) {
                        item.enrollTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_ENROLL_TIME);
                        item.enrollTimeString = getTimeInMillisecond(item.enrollTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_AUTHENTICATE_TIME)) {
                        item.authenticateTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_AUTHENTICATE_TIME);
                        item.authenticateTimeString = getTimeInMillisecond(item.authenticateTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_GET_GSC_DATA_TIME)) {
                        item.getGscDataTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_GET_GSC_DATA_TIME);
                        item.getGscDataTimeString = getTimeInMillisecond(item.getGscDataTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_BIO_ASSAY_TIME)) {
                        item.bioAssayTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_BIO_ASSAY_TIME);
                        item.bioAssayTimeString = getTimeInMillisecond(item.bioAssayTime);
                    }

                    mData.add(item);
                    int mDisplayDataMaxSize = 2;
                    if (null != mConfig && (mConfig.mChipType == Constants.GF_CHIP_5206
                        || mConfig.mChipType == Constants.GF_CHIP_5208)) {
                        mDisplayDataMaxSize = 1;
                    }
                    if (mData.size() > mDisplayDataMaxSize) {
                        mData.remove(0);
                    }
                    mAdapter.notifyDataSetChanged();
                    mListView.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListView.setSelection(mData.size() - 1);
                            mListView.smoothScrollToPosition(mData.size() - 1);
                        }

                    }, 100);
                }
            });
        }
    };
}
