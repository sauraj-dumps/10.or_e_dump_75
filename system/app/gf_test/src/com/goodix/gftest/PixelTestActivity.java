
package com.goodix.gftest;

import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.utils.TestResultChecker.Checker;
import com.goodix.gftest.utils.TestResultChecker.TestResultCheckerFactory;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PixelTestActivity extends Activity {

    private static final String TAG = "PixcelTestActivity";

    private TextView mResultView = null;
    private View mResultDetailView = null;
    private TextView mResultNumView = null;
    private TextView mSuccessTipsView = null;
    private ProgressBar mProgressBar = null;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private Handler mHandler = new Handler();
    private GFConfig mConfig = null;
    private boolean mIsCanceled = false;
    private Checker mTestResultChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pixel_test);

        mGoodixFingerprintManager = new GoodixFingerprintManager(PixelTestActivity.this);
        mConfig = mGoodixFingerprintManager.getConfig();

    }

    public void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mResultView = (TextView) findViewById(R.id.test_result);
        mResultDetailView = findViewById(R.id.test_result_detail);
        mResultNumView = (TextView) findViewById(R.id.test_sensor_result);
        mSuccessTipsView = (TextView) findViewById(R.id.test_sensor_success_tips);

        mSuccessTipsView.setText(getResources().getString(R.string.test_success_tips,
                " <= ", mTestResultChecker.getThresHold().badBointNum));

        mProgressBar = (ProgressBar) findViewById(R.id.testing);
        mResultView.setVisibility(View.INVISIBLE);
        mResultDetailView.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTestResultChecker = TestResultCheckerFactory.getInstance(mConfig.mChipType, mConfig.mChipSeries);

        initView();
        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PIXEL_OPEN, null);
        mIsCanceled = false;
        mHandler.postDelayed(mTimeoutRunnable, Constants.TEST_TIMEOUT_MS);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mHandler.removeCallbacks(mTimeoutRunnable);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Runnable mTimeoutRunnable = new Runnable() {

        @Override
        public void run() {
            mIsCanceled = true;
            mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

            mResultView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            mResultView.setText(R.string.timeout);
            mResultView.setTextColor(getResources().getColor(R.color.test_failed_color));
        }

    };

    private void onTestPixel(final HashMap<Integer, Object> result) {
        mResultView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
        mResultDetailView.setVisibility(View.VISIBLE);

        int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
        if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
            errorCode = (Integer) result
                    .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
        }

        int badPixelNum = 999;
        if (result.containsKey(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM)) {
            badPixelNum = (Integer) result
                    .get(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM);
        }

        mResultNumView.setText(String.valueOf(badPixelNum));
        if (mTestResultChecker.checkPixelTestResult(errorCode, badPixelNum)) {
            mResultNumView.setTextColor(getResources()
                    .getColor(R.color.test_succeed_color));

            mResultView.setText(R.string.test_succeed);
            mResultView.setTextColor(getResources()
                    .getColor(R.color.test_succeed_color));
            return;
        } else {
            mResultNumView.setTextColor(getResources().getColor(
                    R.color.test_failed_color));
            mResultView.setText(R.string.test_failed);
            mResultView.setTextColor(getResources()
                    .getColor(R.color.test_failed_color));
        }

    }

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {
        @Override
        public void onTestCmd(int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (result == null || cmdId != Constants.CMD_TEST_PIXEL_OPEN) {
                return;
            }

            mHandler.removeCallbacks(mTimeoutRunnable);
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    Log.d(TAG, "onTestCmd result");
                    if (mIsCanceled) {
                        return;
                    }
                    onTestPixel(result);
                }

            });

        }
    };
}
