
package com.goodix.gftest;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestResultParser;

public class SpiPerformanceTestActivity extends Activity {

    private static final String TAG = "SpiPerformanceTestActivity";

    private ArrayList<SpiPerformanceTestItem> mData = new ArrayList<SpiPerformanceTestItem>();
    private ListView mListView = null;
    private TextView mResultView = null;
    private TestAdapter mAdapter = null;
    private Button mTestAgainBtn = null;
    private ProgressBar mProgressBar = null;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private Handler mHandler = new Handler();
    private boolean mIsCanceled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spi_performance_test);

        mGoodixFingerprintManager = new GoodixFingerprintManager(SpiPerformanceTestActivity.this);

        initView();
    }

    public void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mResultView = (TextView) findViewById(R.id.test_result);
        mListView = (ListView) findViewById(R.id.listview);
        mAdapter = new TestAdapter();
        mListView.setAdapter(mAdapter);

        mProgressBar = (ProgressBar) findViewById(R.id.testing);
        mTestAgainBtn = (Button) findViewById(R.id.test_again);
        mTestAgainBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mProgressBar.setVisibility(View.VISIBLE);
                mResultView.setVisibility(View.INVISIBLE);

                mIsCanceled = false;
                mHandler.removeCallbacks(mTimeoutRunnable);
                mHandler.postDelayed(mTimeoutRunnable, Constants.TEST_TIMEOUT_MS);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SPI_PERFORMANCE, null);
                mTestAgainBtn.setEnabled(false);
            }
        });
        mTestAgainBtn.setEnabled(false);
        mResultView.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SPI_PERFORMANCE, null);

        mIsCanceled = false;
        mHandler.postDelayed(mTimeoutRunnable, Constants.TEST_TIMEOUT_MS);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mHandler.removeCallbacks(mTimeoutRunnable);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);

        finish();
    }

    private void showTimeOutUI() {
        mResultView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
        mResultView.setText(R.string.timeout);
        mResultView
                .setTextColor(getResources().getColor(R.color.test_failed_color));
        mTestAgainBtn.setEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class TestAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mData != null ? mData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mData != null ? mData.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(SpiPerformanceTestActivity.this).inflate(
                        R.layout.item_spi_performance_test, null);
                holder = new Holder();
                holder.getModeTimeView = (TextView) convertView.findViewById(R.id.get_mode_time);
                holder.getFWVersionTimeView = (TextView) convertView
                        .findViewById(R.id.get_fw_version_time);
                holder.getImageTimeTitleView = (TextView) convertView
                        .findViewById(R.id.get_image_time_title);
                holder.getImageTimeView = (TextView) convertView.findViewById(R.id.get_image_time);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            SpiPerformanceTestItem item = mData.get(position);
            holder.getModeTimeView.setText(item.getModeTimeString);
            holder.getFWVersionTimeView.setText(item.getFWVersionTimeString);
            holder.getImageTimeTitleView.setText(getResources().getString(
                    R.string.test_spi_performance_get_rawdata, item.rawDataLen));
            holder.getImageTimeView.setText(item.getImageTimeString);

            return convertView;
        }

        private class Holder {
            TextView getTimestampTimeView;
            TextView getModeTimeView;
            TextView getFWVersionTimeView;
            TextView getImageTimeTitleView;
            TextView getImageTimeView;
        }
    }

    private String getTimeInMillisecond(int time) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(time));
        sb.append("ms");
        return sb.toString();
    }

    private class SpiPerformanceTestItem {
        int getTimestampTime;
        int getModeTime;
        int getFWVersionTime;
        int getImageTime;
        int rawDataLen;
        String fwVersion;
        String getModeTimeString;
        String getFWVersionTimeString;
        String getImageTimeString;
    }

    private Runnable mTimeoutRunnable = new Runnable() {

        @Override
        public void run() {
            mIsCanceled = true;
            mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
            showTimeOutUI();
        }

    };

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {
        @Override
        public void onTestCmd(int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (result == null || cmdId != Constants.CMD_TEST_SPI_PERFORMANCE) {
                return;
            }

            mHandler.removeCallbacks(mTimeoutRunnable);
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mIsCanceled) {
                        return;
                    }

                    mTestAgainBtn.setEnabled(true);
                    mResultView.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.INVISIBLE);

                    int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
                        errorCode = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
                    }
                    SpiPerformanceTestItem item = new SpiPerformanceTestItem();
                    if (result.containsKey(TestResultParser.TEST_TOKEN_GET_DR_TIMESTAMP_TIME)) {
                        item.getTimestampTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_GET_DR_TIMESTAMP_TIME);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_GET_MODE_TIME)) {
                        item.getModeTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_GET_MODE_TIME);
                        item.getModeTimeString = getTimeInMillisecond(item.getModeTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_GET_FW_VERSION_TIME)) {
                        item.getFWVersionTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_GET_FW_VERSION_TIME);
                        item.getFWVersionTimeString = getTimeInMillisecond(item.getFWVersionTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_GET_IMAGE_TIME)) {
                        item.getImageTime = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_GET_IMAGE_TIME);
                        item.getImageTimeString = getTimeInMillisecond(item.getImageTime);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_RAW_DATA_LEN)) {
                        item.rawDataLen = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_RAW_DATA_LEN);
                    }
                    if (result.containsKey(TestResultParser.TEST_TOKEN_FW_VERSION)) {
                        item.fwVersion = (String) result
                                .get(TestResultParser.TEST_TOKEN_FW_VERSION);
                    }

                    if (0 == errorCode && null != item.fwVersion) {
                        mResultView.setText(R.string.test_succeed);
                        mResultView.setTextColor(getResources()
                                .getColor(R.color.test_succeed_color));
                    } else {
                        mResultView.setText(R.string.test_failed);
                        mResultView.setTextColor(getResources()
                                .getColor(R.color.test_failed_color));
                        return;
                    }

                    mData.add(item);
                    if (mData.size() > 4) {
                        mData.remove(0);
                    }
                    mAdapter.notifyDataSetChanged();
                    mListView.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mListView.setSelection(mData.size() - 1);
                            mListView.smoothScrollToPosition(mData.size() - 1);
                        }

                    }, 100);
                }

            });

        }
    };
}
