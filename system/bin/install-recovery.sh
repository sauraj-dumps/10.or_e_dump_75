#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:31634732:2d13d02ee35319cad8af481ab1c0d56238e26e62; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:29205800:7ee3e13a323cae5bb59b50a1ee70350d59ee9fff EMMC:/dev/block/bootdevice/by-name/recovery 2d13d02ee35319cad8af481ab1c0d56238e26e62 31634732 7ee3e13a323cae5bb59b50a1ee70350d59ee9fff:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
